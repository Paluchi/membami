package home_package

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import com.sonalipaluchi.membami.R
import bills_package.Sonali_AddBill
import kotlinx.android.synthetic.main.fragment_home.*
import notes_package.Sonali_Add_Notes

class Home : Fragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

     override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    //context arrises when the View is created
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab_menu?.setClosedOnTouchOutside(true)
        fab_listeners()

        //val temp = view?.findViewById<com.github.clans.fab.FloatingActionButton>(R.id.fab_createNote)





    }
/** TODO unknown
    fun getsonalipop(view: View){
        val window = PopupWindow(view.context)
        val layout_view = layoutInflater.inflate(R.layout.sonali_popout, null)
        window.contentView = layout_view
        window.setFocusable(true)
        window.update()
        //populateDDL(view)

        val btnkill = layout_view.findViewById<FloatingActionButton>(R.id.sonali_popout_x)
        btnkill.setOnClickListener {
            window.dismiss()
        }
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.showAtLocation(view,17,1,1)
    }*/
    private fun fab_listeners()
    {
        fab_createBillNote?.setOnClickListener{
           fab_menu.close(true)
            val intent = Intent(view!!.context, Sonali_AddBill::class.java)
            intent.putExtra("extraData", getString(R.string.new_))
            startActivity(intent)

        }

        fab_createNote?.setOnClickListener{
            fab_menu.close(true)
            val intent = Intent(view!!.context, Sonali_Add_Notes::class.java)
            intent.putExtra("Notes_MOD", "edit")
            startActivity(intent)


        }

    }


    }
