package notes_package

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.sonalipaluchi.membami.R

class NotesViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    var notes_name: TextView? = null
    var notes_description : TextView? = null
    var notez_id : TextView? = null
    var notez_mod_date : TextView? = null
    var znotesRelative : RelativeLayout? =null

    init {

        notes_name = itemView.findViewById<TextView>(R.id.notestitile)
        notes_description = itemView.findViewById<TextView>(R.id.notesdesc)
        notez_id = itemView.findViewById<TextView>(R.id.notes_database_id)
        notez_mod_date = itemView.findViewById<TextView>(R.id.notesmoddate)
        znotesRelative = itemView.findViewById<RelativeLayout>(R.id.testRelative)
    }

}