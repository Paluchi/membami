package notes_package

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.ArrayList
import kotlinx.android.synthetic.main.fragment_notes.*
import com.sonalipaluchi.membami.*


class Notes : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var noteAdapter : NotesRecyclerAdapter? = null
    var sonaliList = ArrayList<NotesDetails>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?  {//= inflater.inflate(R.layout.fragment_notes, container, false)// {
        // Inflate the layout for this fragment
        val sonali_view= inflater.inflate(R.layout.fragment_notes, container, false)

        return sonali_view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializer()

    }


    override fun onResume() {
        super.onResume()
        initializer()
    }

    private fun populateList():ArrayList<NotesDetails>{

        var noteList = ArrayList<NotesDetails>()

        val dbHandler = Retain_Information(view!!.context, null)
        val cursor = dbHandler.getAllNotes()

        if (cursor!!.moveToFirst() )
        {
            var i = 1

            noteList.add(
                NotesDetails(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.TITLE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.DESCRIPTION)),
                    cursor.getInt(cursor.getColumnIndex(Retain_Information.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.MOD_DATE))
                )
            )
            while(cursor.moveToNext())
            {
                noteList.add(
                    NotesDetails(
                        cursor.getString(cursor.getColumnIndex(Retain_Information.TITLE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.DESCRIPTION)),
                        cursor.getInt(cursor.getColumnIndex(Retain_Information.COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.MOD_DATE))

                    )
                )

                i++
            }
            cursor.close()
        }
        return noteList
    }

    private fun sonali_Notes_Listeners()
    {
        fab_createNotes?.setOnClickListener{
            val intent = Intent(view!!.context, Sonali_Add_Notes::class.java)
            intent.putExtra("Notes_MOD", "edit")
            startActivity(intent)
           // onDestroy()

        }
    }

    private fun initializer()
    {
        val db = Retain_Information(context!!, null)
        val cursor = db.getSettings()
        if(cursor.moveToFirst())
        {
            GlobalVariable.GLB_CUR = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_CURRENCY))
            GlobalVariable.GLB_UNIT = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_UNIT))
        }

        recyclerView = view?.findViewById<RecyclerView>(R.id.add_notes_recyclerView)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        recyclerView!!.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        //recyclerView!!.layoutManager = LinearLayoutManager(view!!.context, LinearLayoutManager.VERTICAL, false)
        sonaliList = populateList()
        noteAdapter = NotesRecyclerAdapter(view!!.context, sonaliList)
        recyclerView!!.adapter = noteAdapter

        checkIfEmptyList(sonaliList)
        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !fab_createNotes.isShown())
                    fab_createNotes.show(true)
                else if (dy > 0 && fab_createNotes.isShown())
                    fab_createNotes.hide(true)
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })





        sonali_Notes_Listeners()
    }
    private fun checkIfEmptyList(list : ArrayList<NotesDetails>)
    {
        if(list.size <= 0)
        {
            placeholderImageNotes.visibility = View.VISIBLE
            add_notes_recyclerView.visibility =View.GONE
            notes_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!, R.color.sonaliWhite))
        }else
        {
            placeholderImageNotes.visibility = View.GONE
            add_notes_recyclerView.visibility =View.VISIBLE
            notes_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!,R.color.sonaliGrey))
        }

    }

}
