package notes_package

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.menu.MenuBuilder
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.sonalipaluchi.membami.MainDelete
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.activity_notes_view.*


class Notes_View :AppCompatActivity(){

    var extra = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_view)
        extra = intent.getStringExtra("Notes_position")
        initializer(extra.toInt())
    }
    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (menu is MenuBuilder) {

            menu.setOptionalIconsVisible(true)
            //setOptionalIconsVisible(true)
        }
        menuInflater.inflate(R.menu.menu_home, menu)

        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        //return super.onOptionsItemSelected(item)
        when(item!!.itemId){
            R.id.popout_edit -> {
                //Toast.makeText(this, "EDIT", Toast.LENGTH_LONG).show()
                var intent = Intent(this, Sonali_Add_Notes::class.java)
                intent.putExtra("Notes_MOD", extra)
                startActivity(intent)
                onBackPressed()

            }
            R.id.popout_cancel -> {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
                onBackPressed()
            }
            R.id.popout_delete -> {
                val intent = Intent(this, MainDelete::class.java)
                intent.putExtra("DELETE_ID", extra)
                intent.putExtra("DELETE_TYPE", getString(R.string.tab_notes))
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initializer( value : Int)
    {
        val db = Retain_Information(this, null)
        val cursor = db.searchNote(value)
        if(cursor!!.moveToFirst())
        {
            notes_popout_title.text = cursor.getString(cursor.getColumnIndex(Retain_Information.TITLE)).capitalize()
            notes_popout_details.text = cursor.getString(cursor.getColumnIndex(Retain_Information.DESCRIPTION))
            notes_popout_mod_date.text = cursor.getString(cursor.getColumnIndex(Retain_Information.MOD_DATE))
           // getSupportActionBar()?.setTitle(notes_popout_title.text)
            supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>${notes_popout_title.text.toString().capitalize()}</font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
            //( Html.fromHtml("<small>" +(notes_popout_title.text).toString().capitalize() +"</small>" ) )
            //supportActionBar?.subtitle =( Html.fromHtml("<small>" +(notes_popout_title.text).toString().capitalize() +"</small>" ) )

        }

    }
    private fun delete_Note(){
        val db = Retain_Information(this, null)
        if(db.deleteNote(extra.toInt())) {
            println("<<<<<<TRUE>>>>>>>>\n\n")
            Toast.makeText(this, "Deleted", Toast.LENGTH_LONG).show()
            onBackPressed()
        }else
            println("<<<<<<<<<<<<<NOT DELETED >>>>>>>>>>>>>>\n\n")
    }



}
