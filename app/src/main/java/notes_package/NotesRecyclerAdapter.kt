package notes_package

import android.support.v7.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.view.LayoutInflater
import android.view.ViewGroup
import com.sonalipaluchi.membami.R
import android.widget.*
import com.sonalipaluchi.membami.General_Functions


class NotesRecyclerAdapter(ctx: Context, private val imageModelArrayList: ArrayList<NotesDetails>) : RecyclerView.Adapter<NotesViewHolder>() {

    private val inflater: LayoutInflater
    private var relativeLayout:RelativeLayout? = null
    val function = General_Functions()


    init {

        inflater = LayoutInflater.from(ctx)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {

        val view = inflater.inflate(R.layout.notes_layout, parent, false)
        relativeLayout?.rootView?.findViewById<RelativeLayout>(R.id.notes_recycler_frame)

        return NotesViewHolder(view)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        // holder?.radio_image?.setImageBitmap(imageModelArrayList[position].image)
        holder.notes_description!!.text = imageModelArrayList[position].notes_desc
        holder.notes_name!!.text = imageModelArrayList[position].notes_title.capitalize()
        holder.notez_id!!.text = imageModelArrayList[position].notes_id.toString()
        holder.notez_mod_date!!.text = imageModelArrayList[position].notes_mod_date
        holder.znotesRelative?.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, function.getRandomColor()))

        holder.itemView.setOnClickListener{
            var intent =Intent(holder.itemView.context, Notes_View::class.java)
            intent.putExtra("Notes_position", (imageModelArrayList[position].notes_id).toString())
            startActivity(holder.itemView.context, intent,null)
        }

    }

    override fun getItemCount(): Int {
        return imageModelArrayList.size
    }

    /**val intent = Intent(holder.itemView.context, Sonali_Add_Notes::class.java)
    intent.putExtra("", "")
    startActivity(holder.itemView.context,intent, null)
     */


}