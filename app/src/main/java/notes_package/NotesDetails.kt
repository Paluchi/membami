package notes_package

class NotesDetails {

    var notes_title: String = ""

    var notes_desc: String = ""
    var notes_mod_date = ""

    var notes_id : Int =0

    constructor(title: String, desc: String, mod_date:String){
        this.notes_title = title
        this.notes_desc = desc
        this.notes_mod_date = mod_date
    }
    constructor(title: String, desc: String, id :Int){
        this.notes_title = title
        this.notes_desc = desc
        this.notes_id = id
    }
    constructor(title: String, desc: String, id :Int , mod_date:String){
        this.notes_title = title
        this.notes_desc = desc
        this.notes_id = id
        this.notes_mod_date = mod_date
    }
    /**constructor(title: String, desc: String, mod_date :String){
        this.notes_title = title
        this.notes_desc = desc
        this.notes_mod_date = mod_date
    }*/
    constructor()
    {
        this.notes_desc = ""
        this.notes_title=""
        this.notes_id = 0
        this.notes_mod_date =""
    }
    fun clear()
    {
        this.notes_title=""
        this.notes_desc=""
        this.notes_id = 0
        this.notes_mod_date = ""
    }

    fun getNotesTitle(): String {
        return notes_title
    }

    fun setNotesTitle(name: String) {
        this.notes_title = name
    }

    fun getNotesDesc(): String {
        return notes_desc
    }

    fun setNotesDesc(desc: String){
        this.notes_desc = desc
    }

    fun getNotesId(): Int{
        return this.notes_id
    }
    fun setNotesId(id: Int)
    {
        this.notes_id = id

    }
    fun getNotes_Mod_Note():String{
        return this.notes_mod_date
    }


}