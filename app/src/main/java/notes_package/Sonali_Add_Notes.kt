package notes_package

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.text.Editable
import android.text.TextUtils
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.Images_Camera
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.activity_sonali__add__notes.*

class Sonali_Add_Notes : AppCompatActivity() {
    var highlight_status : Boolean = false
    var func = General_Functions()
    var status= "8"
    var sent_data = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sonali__add__notes)
        notesDesc?.setSelection(0)
        sent_data = intent.getStringExtra("Notes_MOD")
       /** if (sent_data == "edit")
        {

            supportActionBar?.title = "New Note"
        }*/

        updateView(sent_data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_note, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item!!.itemId)
        {
            R.id.menu_add_note_save ->{

                if(!fieldEmptyCheck())
                {
                    save(sent_data)
                    onBackPressed()
                    finish()
                }

            }
            R.id.menu_add_note_cancel->{
                finish()
            }
            R.id.menu_add_note_camera->{
                startActivity(Intent(this, Images_Camera::class.java))
            }
            R.id.menu_add_note_images->{
                startActivity(Intent(this, Images_Camera::class.java))
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun fieldEmptyCheck(): Boolean{
        var result = false
        if(TextUtils.isEmpty(notesDesc.text.toString()))
        {
            notesDesc.error = getString(R.string.notEmpty)
            result = true
        }
        if(TextUtils.isEmpty(notesTitle.text.toString()))
        {
            notesTitle.error = getString(R.string.notEmpty)
            result = true
        }

        return result
    }

    private fun updateView(intent_val : String ){

        if (intent_val != "edit")
        {
            val db = Retain_Information(this, null)
            var cursor = db.searchNote(intent_val.toInt())
            if(cursor!!.moveToFirst())
            {
                var title = findViewById<EditText>(R.id.notesTitle)

                title.text = Editable.Factory.getInstance().newEditable(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.TITLE))
                )
                notesDesc.text = Editable.Factory.getInstance().newEditable(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.DESCRIPTION))
                )

                supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>Edit Note </font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
            }
            status ="1"

        }else
        {
            supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>New Note </font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }

    private fun save(id :String)
    {
        val db = Retain_Information(this, null)
        if(status in "1")
        {
            db.update(NotesDetails(notesTitle.text.toString(),
                notesDesc.text.toString(),
                func.getDate()),
                id.toInt()
            )
            var update =Toast.makeText(this, "Updated", Toast.LENGTH_LONG)
            update.setGravity(Gravity.BOTTOM,0,0)
            update.show()
        }else{
            db.insertNote(NotesDetails(notesTitle.text.toString(),
                notesDesc.text.toString(),
                func.getDate())
            )
        }

        db.close()
    }

}

