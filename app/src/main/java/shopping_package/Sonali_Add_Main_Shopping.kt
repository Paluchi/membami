package com.sonalipaluchi.membami

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.text.Editable
import android.text.TextUtils
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_sonali__add__main__shopping.*
import shopping_package.shopping_recycler_package.Shopping_Main_Data

class Sonali_Add_Main_Shopping : AppCompatActivity() {

    val functions = General_Functions()
    var status = ""
    var title = ""
    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sonali__add__main__shopping)
        status = intent.getStringExtra("ADD_SHOPPING_STATUS")
        id = intent.getStringExtra("ADD_SHOPPING_ID")
        title = intent.getStringExtra("ADD_SHOPPING_TITLE")


        var root = window.decorView.rootView as ViewGroup
        applyDim(root, 0.5f)

        initializer()
    }

    private fun popupWindow()
    {

        val view = this.layoutInflater.inflate(R.layout.shopping_add_popout_endpoint, null)
        val popupWindow = PopupWindow(
            view,
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        var root = window.decorView.rootView as ViewGroup
        applyDim(root, 0.9f)

        //popupWindow.showAsDropDown(holder.itemView.rootView)
        popupWindow.height = 600


        val cbutton = view.findViewById<Button>(R.id.add_main_shoppingButtonCancel)
        val sbutton = view.findViewById<Button>(R.id.add_main_shoppingButtonSave)
        val popTitle = view.findViewById<TextView>(R.id.add_main_shoppingTitle)
        val ans = view.findViewById<EditText>(R.id.main_add_shopping_Title_Value)
        popTitle.text ="THIS iS ME"

        cbutton?.setOnClickListener {
            popupWindow.dismiss()
        }
        sbutton?.setOnClickListener {
            val db = Retain_Information(this, null)
            if(!fieldEmptyCheck(ans)){
                db.insertMainShopping(
                    Shopping_Main_Data( ans.text.toString(),
                        functions.getDate(),
                        "0",
                        "0",
                        "0",
                        "0"
                    )
                )
            }
            db.close()
            popupWindow.dismiss()
        }
        popupWindow.setOnDismissListener {
            // Toast.makeText(this, "Popup closed", Toast.LENGTH_SHORT).show()
            clearDim(root)
            finish()
        }
        popupWindow.isFocusable = true
        popupWindow.update()
        popupWindow.showAtLocation(addShoppingConstraint, Gravity.CENTER, 0, 0)
    }
    private fun fieldEmptyCheck(temp : EditText): Boolean{
        var result = false
        //val temp = view.findViewById<EditText>(R.id.add_main_shoppingTextValue)
        if(TextUtils.isEmpty(temp.text.toString()))
        {
            temp.error = getString(R.string.notEmpty)
            result = true
        }

        return result
    }
    fun applyDim(parent: ViewGroup, dimAmount: Float) {
        val dim = ColorDrawable(Color.BLACK)
        dim.setBounds(0, 0, parent.width, parent.height)
        dim.alpha = (255 * dimAmount).toInt()

        val overlay = parent.overlay
        overlay.add(dim)
    }

    fun clearDim(parent: ViewGroup) {
        val overlay = parent.overlay
        overlay.clear()
    }

    private fun initializer()
    {
        if(status !in getString(R.string.new_))
        {
            main_add_shopping_Title_Value.text = Editable.Factory.getInstance().newEditable(title)
            supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>Edit List </font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
        }else{
            supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>New List </font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
        sonaliListeners()
    }

    fun sonaliListeners() {
        add_main_shoppingButtonCancel.setOnClickListener {
            finish()
        }

        add_main_shoppingButtonSave.setOnClickListener {
            val db = Retain_Information(this, null)
            if (!fieldEmptyCheck(main_add_shopping_Title_Value)) {
                if(status in getString(R.string.new_))
                {
                    db.insertMainShopping(Shopping_Main_Data(
                        main_add_shopping_Title_Value.text.toString(),
                        functions.getDate(),
                        "0",
                        "0",
                        "0",
                        "0"
                    )
                    )
                    val sonId = db.getMainShoppingID_ForTItle()
                    db.createList(sonId)

                }else if (status !in getString(R.string.new_))
                {
                    db.updateMainShopping(
                        Shopping_Main_Data(
                            main_add_shopping_Title_Value.text.toString(),
                            functions.getDate(),
                            "0",
                            "0",
                            id,
                            "0"
                        )
                    )
                }
            }
            db.close()
            onBackPressed()
        }
    }
}

