package shopping_package

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import com.sonalipaluchi.membami.Sonali_Add_Main_Shopping
import kotlinx.android.synthetic.main.fragment_shopping.*
import shopping_package.shopping_recycler_package.Shopping_Main_Data
import shopping_package.shopping_recycler_package.Shopping_Main_RecyclerAdapter

class Shopping : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var noteAdapter : Shopping_Main_RecyclerAdapter? = null
    var sonaliList = ArrayList<Shopping_Main_Data>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       // return inflater!!.inflate(R.layout.fragment_shopping, container, false)
        val sonali_view= inflater.inflate(R.layout.fragment_shopping, container, false)

        return sonali_view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializer()

    }


    override fun onResume() {
        super.onResume()
        initializer()
    }

    private fun populateList():ArrayList<Shopping_Main_Data>{
        //val list = List<NotesDetails>()

        var shopList = ArrayList<Shopping_Main_Data>()

         val dbHandler = Retain_Information(view!!.context, null)
        val cursor = dbHandler.getAllMainShopping()

        if (cursor!!.moveToFirst() )
        {
            shopList.add(
                Shopping_Main_Data(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_TITLE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_DATE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ITEM_DONE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ITEM_TOTAL)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ID)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_TOTAL))

                )
            )
            while(cursor.moveToNext())
            {
                shopList.add(
                    Shopping_Main_Data(
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_DATE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ITEM_DONE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ITEM_TOTAL)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_ID)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.SHOPPING_TOTAL))
                    )
                )
            }
            cursor.close()
        }
        /***/



        return shopList

    }

    private fun sonali_Notes_Listeners()
    {
        shopping_add_fab?.setOnClickListener{
            val intent = Intent(view!!.context, Sonali_Add_Main_Shopping::class.java)
            intent.putExtra("ADD_SHOPPING_STATUS", getString(R.string.new_))
            intent.putExtra("ADD_SHOPPING_ID", getString(R.string.new_))
            intent.putExtra("ADD_SHOPPING_TITLE", getString(R.string.new_))
            startActivity(intent)

        }/***/
    }

    private fun initializer()
    {

        val db = Retain_Information(context!!, null)
        val cursor = db.getSettings()
        if(cursor.moveToFirst())
        {
            GLB_CUR = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_CURRENCY))
            GLB_UNIT = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_UNIT))
        }

        cursor.close()
        db.close()

        recyclerView = view?.findViewById<RecyclerView>(R.id.recycler_shopping)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        recyclerView!!.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        //recyclerView!!.layoutManager = LinearLayoutManager(view!!.context, LinearLayoutManager.VERTICAL, false)
        sonaliList = populateList()
        noteAdapter = Shopping_Main_RecyclerAdapter(view!!.context, sonaliList)
        recyclerView!!.adapter = noteAdapter
        checkIfEmptyList(sonaliList)

        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !shopping_add_fab.isShown())
                    shopping_add_fab.show(true)
                else if (dy > 0 && shopping_add_fab.isShown())
                    shopping_add_fab.hide(true)
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })



        sonali_Notes_Listeners()
    }

    private fun checkIfEmptyList(list : ArrayList<Shopping_Main_Data>)
    {
        if(list.size <= 0)
        {
            placeholderImage.visibility = View.VISIBLE
            recycler_shopping.visibility =View.GONE
            shopping_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!,R.color.sonaliWhite))
        }else
        {
            placeholderImage.visibility = View.GONE
            recycler_shopping.visibility =View.VISIBLE
            shopping_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!,R.color.sonaliGrey))
        }

    }
}