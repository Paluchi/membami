package shopping_package.shopping_recycler_package

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.sonalipaluchi.membami.R

class Shopping_Recycler_ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shopping_viewholder_title: TextView? = null
    var shopping_viewholder_date : TextView? = null
    var shopping_viewholder_itemCount : TextView? = null
    var shopping_viewholder_id : TextView? = null
    var shopping_viewholder_button: TextView? = null
    var shopping_viewholder_shoppingMain_relative : RelativeLayout? = null
    var shopping_viewholder_total : TextView? = null

    init {

        shopping_viewholder_title = itemView.findViewById<TextView>(R.id.shopping_Title)
        shopping_viewholder_date = itemView.findViewById<TextView>(R.id.shopping_createdDate)
        shopping_viewholder_itemCount = itemView.findViewById<TextView>(R.id.shopping_item_count)
        shopping_viewholder_id = itemView.findViewById<TextView>(R.id.shopping_id)
        shopping_viewholder_button = itemView.findViewById(R.id.shopping_main_optionsButton)
        shopping_viewholder_shoppingMain_relative = itemView.findViewById(R.id.shoppingMain_relative)
        shopping_viewholder_total = itemView.findViewById(R.id.shopping_list_Total)

    }

}