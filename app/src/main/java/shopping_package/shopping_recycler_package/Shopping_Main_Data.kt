package shopping_package.shopping_recycler_package

class Shopping_Main_Data
{
    internal var shopping_Main_Data_title = ""
    internal var shopping_Main_Data_date= ""
    internal var shopping_Main_Data_itemDone= ""
    internal var shopping_Main_Data_itemTotal= ""
    internal var shopping_Main_Data_id = ""
    internal var shopping_Main_Data_total = ""

    constructor(title: String, date:String, itemdone:String, itemTotal :String, id:String, total : String)
    {
        this.shopping_Main_Data_title=title
        this.shopping_Main_Data_date = date
        this.shopping_Main_Data_itemDone = itemdone
        this.shopping_Main_Data_itemTotal = itemTotal
        this.shopping_Main_Data_id = id
        this.shopping_Main_Data_total = total

    }

}