package shopping_package.shopping_recycler_package

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.sonalipaluchi.membami.*
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import shopping_package.shopping_details_view.Shopping_Details_View

class Shopping_Main_RecyclerAdapter(ctx: Context, private val shoppingArrayList: ArrayList<Shopping_Main_Data>) : RecyclerView.Adapter<Shopping_Recycler_ViewHolder>() {

    private val inflater: LayoutInflater
    private var relativeLayout: RelativeLayout? = null
    private val function = General_Functions()
    private val glb      = GlobalVariable()


    init {

        inflater = LayoutInflater.from(ctx)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Shopping_Recycler_ViewHolder {

        val view = inflater.inflate(R.layout.shopping_recycler_row_layout, parent, false)
        relativeLayout?.rootView?.findViewById<RelativeLayout>(R.id.shopping_recycler_frame)

        return Shopping_Recycler_ViewHolder(view)
    }

    override fun onBindViewHolder(holder: Shopping_Recycler_ViewHolder, position: Int) {
        // holder?.radio_image?.setImageBitmap(imageModelArrayList[position].image)
        holder.shopping_viewholder_title !!.text = shoppingArrayList[position].shopping_Main_Data_title.capitalize()
        holder.shopping_viewholder_date!!.text = shoppingArrayList[position].shopping_Main_Data_date
        holder.shopping_viewholder_itemCount!!.text = "${shoppingArrayList[position].shopping_Main_Data_itemDone}/${shoppingArrayList[position].shopping_Main_Data_itemTotal}"
        holder.shopping_viewholder_id!!.text = shoppingArrayList[position].shopping_Main_Data_id
        holder.shopping_viewholder_shoppingMain_relative?.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, function.getRandomColor()))
        holder.shopping_viewholder_total!!.text = function.formatMOney(shoppingArrayList[position].shopping_Main_Data_total.toDouble(),
            GLB_CUR,GLB_UNIT)
       // Log.e("LANGUAGE->", glb.globalLanguage)
       // Log.w("COUNTRY_>",glb.globalCountry)
        holder.itemView.setOnClickListener(View.OnClickListener {
            var intent = Intent(holder.itemView.context, Shopping_Details_View::class.java)
            //intent.putExtra("Bill_Details", (imageModelArrayList[position].bills_recycler_data_class_title))
            //intent.putExtra("Bills_Details_Title", imageModelArrayList[position].billTitle)
            intent.putExtra("SHOPPING_LIST_ID", shoppingArrayList[position].shopping_Main_Data_id.toString())
            intent.putExtra("SHOPPING_LIST_TITLE", shoppingArrayList[position].shopping_Main_Data_title)
            startActivity(holder.itemView.context, intent,null)

        })
            holder.shopping_viewholder_button?.setOnClickListener(View.OnClickListener {

            val pop = PopupMenu(holder.itemView.context, holder.itemView, Gravity.RIGHT)
            pop.setOnMenuItemClickListener {
                    item: MenuItem? ->
                when (item?.itemId)
                {
                    R.id.menu_edit_shopping ->{
                        val intent = Intent(holder.itemView.context, Sonali_Add_Main_Shopping::class.java)
                        intent.putExtra("ADD_SHOPPING_STATUS", holder.itemView.context.getString(R.string.Bill))
                        intent.putExtra("ADD_SHOPPING_ID", shoppingArrayList[position].shopping_Main_Data_id)
                        intent.putExtra("ADD_SHOPPING_TITLE", shoppingArrayList[position].shopping_Main_Data_title)
                        startActivity(holder.itemView.context, intent, null)

                        true
                    }
                    R.id.menu_delete_shopping ->{

                        val intent = Intent(holder.itemView.context, MainDelete::class.java)
                        intent.putExtra("DELETE_ID", shoppingArrayList[position].shopping_Main_Data_id)
                        intent.putExtra("DELETE_TYPE", holder.itemView.context.getString(R.string.tab_shopping))
                        startActivity(holder.itemView.context, intent, null)

                        true
                    }
                    else ->{false}
                }

            }
            pop.inflate(R.menu.menu_shopping_popup)
            try {
                val field = PopupMenu::class.java.getDeclaredField("mPopup")
                field.isAccessible = true
                val mPopup = field.get(pop)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            }catch(e: Exception){
                Log.e("MAIN", "ERROR SHOWING MENU ICONS",e)
            }finally {
                pop.show()
            }
        })

    }

    override fun getItemCount(): Int {
        return shoppingArrayList.size
    }
    /**fun sonaliPopupWindow()
    {
        val tempInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = tempInflater.inflate(R.layout.add_shopping_lists_popup_window, null)
        val price = view.findViewById<EditText>(R.id.shopping_list_price_Value)
        val title = view.findViewById<EditText>(R.id.shopping_list_title_Value)
        val quantity = view.findViewById<EditText>(R.id.shopping_list_quantity_Value)
        val nobtn = view.findViewById<Button>(R.id.shopping_list_button_cancel)
        val yesbtn = view.findViewById<Button>(R.id.shopping_list_button_save)
        val popupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT, true)
        popupWindow.showAtLocation(shopping_details_recyclerView, Gravity.CENTER,0,0)

        yesbtn.setOnClickListener {
            val db = Retain_Information(this, null)
            db.insertList(
                Shopping_List_Data(title.text.toString(),price.text.toString(),shoplistID,quantity.text.toString())
            )
            db.close()
            popupWindow.dismiss()
            onRestart()
        }
        nobtn.setOnClickListener {
            popupWindow.dismiss()
            onRestart()
        }
    }*/

    /**val intent = Intent(holder.itemView.context, Sonali_Add_Notes::class.java)
    intent.putExtra("", "")
    startActivity(holder.itemView.context,intent, null)
     */


}