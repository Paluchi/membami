package shopping_package.shopping_details_view

import android.app.AlertDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.text.HtmlCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.activity_shopping__details__view.*
import shopping_package.shopping_details_view.shopping_details_listRecycler_package.Shopping_List_Data
import shopping_package.shopping_details_view.shopping_details_listRecycler_package.Shopping_List_RecyclerAdapter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.Environment
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class Shopping_Details_View : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var noteAdapter : Shopping_List_RecyclerAdapter? = null
    val functions = General_Functions()
    var sonaliList = ArrayList<Shopping_List_Data>()
    var shoplistID = ""
    var shoplistTitle =""
    var image =Any()
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping__details__view)
        shoplistID = intent.getStringExtra("SHOPPING_LIST_ID")
        shoplistTitle = intent.getStringExtra("SHOPPING_LIST_TITLE")
        //supportActionBar?.title = "${shoplistTitle.capitalize()}'s ${getString(R.string.shoppingList)}"
        supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>${shoplistTitle.capitalize()}</font>", HtmlCompat.FROM_HTML_MODE_LEGACY)
        initializer()
    }

    override fun onRestart() {
        super.onRestart()
        initializer()
    }

    fun initializer()
    {
        recyclerView = findViewById<RecyclerView>(R.id.shopping_details_recyclerView)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        sonaliList = populateList()
        noteAdapter = Shopping_List_RecyclerAdapter(this, sonaliList)
        noteAdapter?.notifyDataSetChanged()
        recyclerView!!.invalidate()
        recyclerView!!.adapter = noteAdapter
        checkIfEmptyList(sonaliList)


        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !add_Shopping_Items_Fab.isShown())
                    add_Shopping_Items_Fab.show(true)
                else if (dy > 0 && add_Shopping_Items_Fab.isShown())
                    add_Shopping_Items_Fab.hide(true)
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
        listeners()

    }

    private fun checkIfEmptyList(list : ArrayList<Shopping_List_Data>)
    {
        if(list.size <= 0)
        {
            placeholderImageShoppingListDetails.visibility = View.VISIBLE
            shopping_details_recyclerView.visibility =View.GONE
            shopping_details_recycler_frame.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.sonaliWhite))
        }else
        {
            placeholderImageShoppingListDetails.visibility = View.GONE
            shopping_details_recyclerView.visibility =View.VISIBLE
            shopping_details_recycler_frame.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.sonaliGrey))
        }

    }

    private fun populateList():ArrayList<Shopping_List_Data>{

        var shopList = ArrayList<Shopping_List_Data>()

        val dbHandler = Retain_Information(this, null)
        val cursor = dbHandler.getAllShoppingList(shoplistID)

        if (cursor!!.moveToFirst() )
        {
           // constructor(title: String, price:String, idTitle:String, quantity :String, status : String, listID: String)
            shopList.add(
                Shopping_List_Data(cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_TITLE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IDTITLE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_UNIT)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_STATUS)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_ID)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IMAGEURI))
                )
            )
            while(cursor.moveToNext())
            {
                shopList.add(
                    Shopping_List_Data(cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_TITLE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IDTITLE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_UNIT)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_STATUS)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_ID)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IMAGEURI))
                    )
                )
            }
            cursor.close()
        }
        return shopList
    }

    fun listeners()
    {
        add_Shopping_Items_Fab.setOnClickListener {

            var intent = Intent(this, Shopping_Details_View_Modifier::class.java)
            intent.putExtra("SHOPPING_LIST_CLASS", Shopping_List_Data(
                getString(R.string.notEmpty), getString(R.string.notEmpty),
                shoplistID, getString(R.string.notEmpty),
                getString(R.string.notEmpty), getString(R.string.new_), getString(R.string.notEmpty),
                getString(R.string.notEmpty)
            ))
            /**intent.putExtra("SHOPPING_LIST_MODIFIER_ID_TITLE", shoplistID)
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_ID", getString(R.string.notEmpty))
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_TITLE", getString(R.string.notEmpty))
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_PRICE", getString(R.string.notEmpty))
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_UNIT",getString(R.string.notEmpty))
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_QUANTITY", getString(R.string.notEmpty))
            intent.putExtra("SHOPPING_LIST_MODIFIER_LIST_STATUS", getString(R.string.new_))*/
            startActivity(intent)

        }
    }

   /** fun sonaliPopupWindow()
    {
        val tempInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = tempInflater.inflate(R.layout.add_shopping_lists_popup_window, null)
        val price = view.findViewById<EditText>(R.id.shopping_list_price_Value)
        val title = view.findViewById<EditText>(R.id.shopping_list_title_Value)
        val quantity = view.findViewById<EditText>(R.id.shopping_list_quantity_Value)
        val nobtn = view.findViewById<Button>(R.id.shopping_list_button_cancel)
        val yesbtn = view.findViewById<Button>(R.id.shopping_list_button_save)
        val spinner = view.findViewById<Spinner>(R.id.shopping_list_spinner_unit)
        val popupWindow = PopupWindow(view,LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT, true)
        popupWindow.showAtLocation(shopping_details_recyclerView, Gravity.CENTER,0,0)
        var units = functions.getUnits(view.context)
        val images = view.findViewById<ImageView>(R.id.shopping_list_image)

        val aAdapter = ArrayAdapter(view.context, R.layout.spinner_row_modifier_no_caps, units)
        spinner?.adapter = aAdapter

        title.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= title.getRight() - title.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {

                    showPictureDialog()
                    if(image == null)
                    {
                        Log.e("THIS IS ", "EMPTY")
                    }else{
                        Log.e("NOT EMPTY", image.toString())
                       // images.setImageURI(image as Uri)
                    }
                    return@OnTouchListener true
                }
            }
            false
        })

       yesbtn.setOnClickListener {
           val db = Retain_Information(this, null)
           if(!fieldEmptyCheck(title))
           {
               if(quantity.text.toString()== "0" || TextUtils.isEmpty(quantity.text.toString()))
                   quantity.text = Editable.Factory.getInstance().newEditable("1")

               if(price.text.toString()== "0" || TextUtils.isEmpty(price.text.toString()))
                   price.text = Editable.Factory.getInstance().newEditable("1")

               db.insertList(
                   Shopping_List_Data((title.text.toString()).trim(),price.text.toString(),shoplistID,quantity.text.toString(),spinner.selectedItem.toString(), "0","0","")
               )
               val cursor = db.getAllShoppingList(shoplistID)
               var total = 0.0f
               if(cursor!!.moveToFirst())
               {
                   total = cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat()
                   while(cursor.moveToNext())
                   {
                       total = total + ( cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat() )
                   }
               }
               db.updateMainShoppingTotal(shoplistID, total.toString())
               db.close()
               popupWindow.dismiss()
               onRestart()
           }
        }
        nobtn.setOnClickListener {
            popupWindow.dismiss()
            onRestart()
        }
    }
   */

    private fun fieldEmptyCheck(text1:EditText): Boolean{
        var result = false
        if(TextUtils.isEmpty(text1.text.toString()))
        {
            text1.error = getString(R.string.notEmpty)
            result = true
        }
        /**if(TextUtils.isEmpty(text2.text.toString()))
        {
            text2.error = getString(R.string.notEmpty)
            result = true
        }
        if(TextUtils.isEmpty(text3.text.toString()))
        {
            text3.error = getString(R.string.notEmpty)
            result = true
        }*/

        return result
    }

    /**TODO AMERA AND CALERRY */
    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {

        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY)
    }
    private fun takePhotoFromCamera() {
       // val permisson = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        if (!functions.getPermission(this, android.Manifest.permission.CAMERA)) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }

    public override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /* if (resultCode == this.RESULT_CANCELED)
         {
         return
         }*/
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                val contentURI = data!!.data
                try
                {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    //val path = saveImage(bitmap)
                   // Log.e("ERROR", path)
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
                    image = functions.bitmapToFile(bitmap,this)
                    Log.e("Path", functions.bitmapToFile(bitmap,this).toString())
                    //shopping_list_image!!.setImageBitmap(bitmap)
                    val sonali = BitmapFactory.decodeFile(functions.bitmapToFile(bitmap,this).toString())
                    //shopping_list_image.setImageBitmap(sonali)

                    /**Glide.with(this)
                    .load(path)
                    .apply(
                    RequestOptions()
                    .placeholder(R.drawable.membami_images_icon_vanilla)
                    .circleCrop()
                    )
                    .error(R.drawable.membami_images_icon_vanilla)
                    .into(testiv)
                     */


                }
                catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            // val temp = thumbnail.toString()
            val uri = functions.bitmapToFile(thumbnail, this)
            image = functions.bitmapToFile(thumbnail, this)

            Log.e("THUMBNAIL", uri.toString())
            Log.e("#####THUTL", uri.toString())
            //val imagebytes = Base64.decode(temp,0)
            // val image = BitmapFactory.decodeByteArray(imagebytes,0,imagebytes.size)

            //shopping_list_image!!.setImageBitmap(thumbnail)
            val sonali = BitmapFactory.decodeFile(functions.bitmapToFile(thumbnail,this).toString())
            //shopping_list_image.setImageBitmap(sonali)


        }
    }
    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString())
        // have the object build the directory structure, if needed.
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


}
