package shopping_package.shopping_details_view.shopping_details_listRecycler_package

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sonalipaluchi.membami.*
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import shopping_package.shopping_details_view.Shopping_Details_View_Modifier
import java.io.File


class Shopping_List_RecyclerAdapter(ctx: Context, private val imageModelArrayList: ArrayList<Shopping_List_Data>) : RecyclerView.Adapter<Shopping_List_ViewHolder>() {

    private val inflater: LayoutInflater
    private var relativeLayout: RelativeLayout? = null
    private val functions = General_Functions()
    private val glb        = GlobalVariable()

    init {

        inflater = LayoutInflater.from(ctx)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Shopping_List_ViewHolder {

        val view = inflater.inflate(R.layout.shoppinglist_row_recycler_view, parent, false)
        relativeLayout?.rootView?.findViewById<RelativeLayout>(R.id.shopping_details_recycler_frame)

        return Shopping_List_ViewHolder(view)
    }

    override fun onBindViewHolder(holder: Shopping_List_ViewHolder, position: Int) {
        // holder?.radio_image?.setImageBitmap(imageModelArrayList[position].image)
        val db = Retain_Information(holder.itemView.context, null)
        var totalitems : Any
        var purchased  : Any
        holder.shopping_list_viewHolder_title!!.text = imageModelArrayList[position].shopping_List_Data_Title.capitalize()
        val total = imageModelArrayList[position].shopping_List_Data_Price.toDouble() * imageModelArrayList[position].shopping_List_Data_Quantity.toDouble()
        holder.shopping_list_viewHolder_price!!.text = functions.formatMOney(total,GLB_CUR,GLB_UNIT)
        holder.shopping_list_viewHolder_id!!.text = imageModelArrayList[position].shopping_List_Data_ListID
        holder.shopping_list_viewholder_quantity!!.text = imageModelArrayList[position].shopping_List_Data_Quantity
        holder.shopping_list_viewholder_unit!!.text = imageModelArrayList[position].shopping_List_Data_Unit
        holder.shopping_list_viewholder_idTitle!!.text = imageModelArrayList[position].shopping_List_Data_IdTitle

        if( imageModelArrayList[position].shopping_List_Data_Image_Uri != holder.itemView.context.getString(R.string.notEmpty))
        {
            Glide.with(holder.itemView.context)
                .load(File(imageModelArrayList[position].shopping_List_Data_Image_Uri))
                .apply(
                    RequestOptions()
                        .optionalCircleCrop()
                )
                .error(R.drawable.membami_camera_icon_weborange)
                .into(holder.shopping_list_viewholder_image_preview!!)
            holder.shopping_list_viewholder_image_preview?.visibility = View.VISIBLE
        } else if (imageModelArrayList[position].shopping_List_Data_Image_Uri != holder.itemView.context.getString(R.string.notEmpty))
        {
            holder.shopping_list_viewholder_image_preview?.visibility = View.GONE
        }


        if(imageModelArrayList[position].shopping_List_Data_Status !in holder.itemView.context.getString(R.string.placeholderNumberOnly))
        {
            holder.shopping_list_viewholder_status?.setImageResource(R.drawable.membami_checkbox_full_icon)

        }else if(imageModelArrayList[position].shopping_List_Data_Status in holder.itemView.context.getString(R.string.placeholderNumberOnly))
        {
            holder.shopping_list_viewholder_status?.setImageResource(R.drawable.membami_checkbox_empty_icon)

        }

        holder.shopping_list_viewholder_status?.setOnClickListener {


            val status = db.getStatusOfListItem( imageModelArrayList[position].shopping_List_Data_IdTitle, imageModelArrayList[position].shopping_List_Data_ListID)
            if (status in holder.itemView.context.getString(R.string.placeholderNumberOnly))
            {
                holder.shopping_list_viewholder_status?.setImageResource(R.drawable.membami_checkbox_full_icon)
                db.updateListCart(imageModelArrayList[position].shopping_List_Data_IdTitle,
                    "1",
                    imageModelArrayList[position].shopping_List_Data_ListID)

            }else if(status !in holder.itemView.context.getString(R.string.placeholderNumberOnly))
            {
                holder.shopping_list_viewholder_status?.setImageResource(R.drawable.membami_checkbox_empty_icon)
                db.updateListCart(imageModelArrayList[position].shopping_List_Data_IdTitle,
                    "0",
                    imageModelArrayList[position].shopping_List_Data_ListID)
            }
            totalitems = db.getTotalItems(imageModelArrayList[position].shopping_List_Data_IdTitle)
            purchased = db.getPurchasedItems(imageModelArrayList[position].shopping_List_Data_IdTitle)
            db.setShoppingItems(imageModelArrayList[position].shopping_List_Data_IdTitle, purchased.toString(), totalitems.toString())


        }
        totalitems = db.getTotalItems(imageModelArrayList[position].shopping_List_Data_IdTitle)
         purchased = db.getPurchasedItems(imageModelArrayList[position].shopping_List_Data_IdTitle)
        db.setShoppingItems(imageModelArrayList[position].shopping_List_Data_IdTitle, purchased.toString(), totalitems.toString())
        //db.close()

        holder.itemView.setOnClickListener{

            val pop = PopupMenu(holder.itemView.context, holder.itemView, Gravity.RIGHT)
            pop.setOnMenuItemClickListener {
                    item: MenuItem? ->
                when (item?.itemId)
                {
                    R.id.menu_edit_shopping_list ->{
                        var intent = Intent(holder.itemView.context, Shopping_Details_View_Modifier::class.java)
                        intent.putExtra("SHOPPING_LIST_CLASS", Shopping_List_Data(
                            imageModelArrayList[position].shopping_List_Data_Title, imageModelArrayList[position].shopping_List_Data_Price,
                            imageModelArrayList[position].shopping_List_Data_IdTitle, imageModelArrayList[position].shopping_List_Data_Quantity,
                            imageModelArrayList[position].shopping_List_Data_Unit, holder.itemView.context.getString(R.string.edit), imageModelArrayList[position].shopping_List_Data_ListID,
                            imageModelArrayList[position].shopping_List_Data_Image_Uri
                        ))
                        holder.itemView.context.startActivity(intent)
                    }
                    R.id.menu_delete_shopping_list ->{

                        val intent = Intent(holder.itemView.context, MainDelete::class.java)
                        intent.putExtra("DELETE_ID", imageModelArrayList[position].shopping_List_Data_IdTitle)
                        intent.putExtra("DELETE_TYPE", imageModelArrayList[position].shopping_List_Data_ListID)
                        //intent.putExtra("DELETE_MISC", )
                        holder.itemView.context.startActivity( intent)
                    }
                    else ->{
                    }
                }


                val dbr = Retain_Information(holder.itemView.context, null)
                totalitems = dbr.getTotalItems(imageModelArrayList[position].shopping_List_Data_IdTitle)
                //purchased = dbr.getPurchasedItems(imageModelArrayList[position].shopping_List_Data_IdTitle,imageModelArrayList[position].shopping_List_Data_ListID)
                true

            }
            pop.inflate(R.menu.menu_shopping_list_popup)
            try {
                val field = PopupMenu::class.java.getDeclaredField("mPopup")
                field.isAccessible = true
                val mPopup = field.get(pop)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            }catch(e: Exception){
                Log.e("MAIN", "ERROR SHOWING MENU ICONS",e)
            }finally {
                pop.show()
            }



        }
        holder.shopping_list_viewholder_image_preview?.setOnClickListener {
            if( imageModelArrayList[position].shopping_List_Data_Image_Uri != holder.itemView.context.getString(R.string.notEmpty))
            {
                val tempInflater = holder.itemView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                var view = tempInflater.inflate(R.layout.membami_image_viewer_popupwindow_layout, null)
                val imgV = view.findViewById<ImageView>(R.id.membami_image_viewer_main_image)
                //val root = view.findViewById<EditText>(R.id.shopping_list_price_Value)
                //val closeBtn = view.findViewById<ImageButton>(R.id.fyuzed_image_viewer_main_image_btn)
                val popupWindow = PopupWindow(view,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    true)
                popupWindow.setAnimationStyle(R.style.MembaMi_IMage_viewer_Animation)
                popupWindow.showAtLocation(view.rootView, Gravity.CENTER,0,0)
                popupWindow.dimBehind()

                Glide.with(holder.itemView.context)
                    .load(File(imageModelArrayList[position].shopping_List_Data_Image_Uri))
                    .fitCenter()
                    .error(R.drawable.membami_camera_icon_weborange)
                    .into(imgV)
                popupWindow.setOnDismissListener {
                    // Toast.makeText(this, "Popup closed", Toast.LENGTH_SHORT).show()

                }
            }
        }

        db.close()

    }
    fun PopupWindow.dimBehind() {
        val container = contentView.rootView
        val context = contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.7f
        wm.updateViewLayout(container, p)
    }

    override fun getItemCount(): Int {
        return imageModelArrayList.size
    }

    /**fun sonaliPopupWindow()
    {
        val tempInflater =  getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = tempInflater.inflate(R.layout.add_shopping_lists_popup_window, null)
        val price = view.findViewById<EditText>(R.id.shopping_list_price_Value)
        val title = view.findViewById<EditText>(R.id.shopping_list_title_Value)
        val quantity = view.findViewById<EditText>(R.id.shopping_list_quantity_Value)
        val nobtn = view.findViewById<Button>(R.id.shopping_list_button_cancel)
        val yesbtn = view.findViewById<Button>(R.id.shopping_list_button_save)
        val popupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT, true)
        popupWindow.showAtLocation(shopping_details_recyclerView, Gravity.CENTER,0,0)

        yesbtn.setOnClickListener {
            val db = Retain_Information(this, null)
            db.insertList(
                Shopping_List_Data(title.text.toString(),price.text.toString(),shoplistID,quantity.text.toString(),"0","0")
            )
            db.close()
            popupWindow.dismiss()
            onRestart()
        }
        nobtn.setOnClickListener {
            popupWindow.dismiss()
            onRestart()
        }
    }




    val intent = Intent(holder.itemView.context, Sonali_Add_Notes::class.java)
    intent.putExtra("", "")
    startActivity(holder.itemView.context,intent, null)
     */


}