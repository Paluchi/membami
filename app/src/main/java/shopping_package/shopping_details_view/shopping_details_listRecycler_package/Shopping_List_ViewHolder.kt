package shopping_package.shopping_details_view.shopping_details_listRecycler_package

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.sonalipaluchi.membami.R

class Shopping_List_ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shopping_list_viewHolder_title: TextView? = null
    var shopping_list_viewHolder_price : TextView? = null
    var shopping_list_viewHolder_id : TextView? = null
    var shopping_list_viewholder_quantity :TextView? =null
    var shopping_list_viewholder_unit   : TextView?=null
    var shopping_list_viewholder_status : ImageButton? =null
    var shopping_list_viewholder_idTitle : TextView? = null
    var shopping_list_viewholder_image_preview : ImageView? = null

    init {

        shopping_list_viewHolder_title = itemView.findViewById<TextView>(R.id.shopping_list_Title)
        shopping_list_viewHolder_price = itemView.findViewById<TextView>(R.id.shopping_list_price)
        shopping_list_viewHolder_id = itemView.findViewById<TextView>(R.id.shopping_list_idList)
        shopping_list_viewholder_quantity = itemView.findViewById<TextView>(R.id.shopping_list_quantity)
        shopping_list_viewholder_unit   = itemView.findViewById(R.id.shopping_list_unit)
        shopping_list_viewholder_status = itemView.findViewById(R.id.shopping_list_Cart_icon_button)
        shopping_list_viewholder_idTitle = itemView.findViewById<TextView>(R.id.shopping_list_idTitle)
        shopping_list_viewholder_image_preview = itemView.findViewById(R.id.shopping_list_image_preview)


    }

}