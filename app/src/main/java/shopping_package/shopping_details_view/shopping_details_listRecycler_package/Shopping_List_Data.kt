package shopping_package.shopping_details_view.shopping_details_listRecycler_package

import java.io.Serializable
class Shopping_List_Data : Serializable
{
    internal var shopping_List_Data_Title = ""
    internal var shopping_List_Data_Price = ""
    internal var shopping_List_Data_IdTitle   = ""
    internal var shopping_List_Data_Quantity = ""
    internal var shopping_List_Data_Status = ""
    internal var shopping_List_Data_ListID   = ""
    internal var shopping_List_Data_Unit       =""
    internal var shopping_List_Data_Image_Uri  = ""

    constructor(title: String, price:String, idTitle:String, quantity :String, unit:String,  status : String, listID: String, image_uri : String)
    {
        this.shopping_List_Data_Title=title
        this.shopping_List_Data_Price = price
        this.shopping_List_Data_IdTitle = idTitle
        this.shopping_List_Data_Quantity = quantity
        this.shopping_List_Data_Unit    = unit
        this.shopping_List_Data_Status = status
        this.shopping_List_Data_ListID =listID
        this.shopping_List_Data_Image_Uri = image_uri
    }
}