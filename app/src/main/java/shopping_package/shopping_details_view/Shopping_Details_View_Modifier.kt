package shopping_package.shopping_details_view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.add_shopping_lists_popup_window.*
import shopping_package.shopping_details_view.shopping_details_listRecycler_package.Shopping_List_Data
import java.io.*
import java.util.*
import android.view.WindowManager
import android.graphics.Matrix
import android.media.ExifInterface


class Shopping_Details_View_Modifier : AppCompatActivity() {

    private var idTitle = ""
    private var listId = ""
    private var listprice = ""
    private var listquantity =""
    private var listunit=""
    private var listtitle = ""
    private var status= ""
    private val GALLERY = 1
    private val CAMERA = 2
    val functions = General_Functions()
    //private String imgPath;
    private var toDelete = ""
    private var imagePath = ""
    private var tempimagePath = ""
    private var imageUri = Uri.parse("/data/0/")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_shopping_lists_popup_window)

        val list = intent.getSerializableExtra("SHOPPING_LIST_CLASS") as Shopping_List_Data
        idTitle = list.shopping_List_Data_IdTitle
        listId = list.shopping_List_Data_ListID
        listprice = list.shopping_List_Data_Price
        listunit = list.shopping_List_Data_Unit
        listquantity = list.shopping_List_Data_Quantity
        listtitle = list.shopping_List_Data_Title
        status = list.shopping_List_Data_Status
        imagePath = list.shopping_List_Data_Image_Uri

        initializer()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        functions.deleteImage(imagePath)
    }

    private fun initializer()
    {
        var array = functions.getUnits(this)
        val Adapter = ArrayAdapter(this, R.layout.spinner_row_modifier_no_caps, array)
        shopping_list_spinner_unit?.adapter = Adapter

        if (status in  getString(R.string.edit))
        {
            shopping_list_title_Value.text = Editable.Factory.getInstance().newEditable(listtitle)
            shopping_list_price_Value.text = Editable.Factory.getInstance().newEditable(listprice)
            shopping_list_quantity_Value.text = Editable.Factory.getInstance().newEditable(listquantity)
            //dueperBillsDDL.setSelection(due_per.indexOf( cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_REAPEAT)) ))
            //val result =
            shopping_list_spinner_unit.setSelection( array.indexOf(listunit))
            Glide.with(this)
                .load(File(imagePath))
                .apply(
                    RequestOptions()
                        .circleCrop()
                )
                .error(R.drawable.membami_images_icon_vanilla)
                .into(shopping_list_image)

            shopping_list_image.visibility = View.VISIBLE


        }

        listeners()
    }

    private fun listeners()
    {
        shopping_list_button_save.setOnClickListener {
            val db = Retain_Information(this, null)
             if(status in getString(R.string.edit)) {
                 if (!fieldEmptyCheck(shopping_list_title_Value)) {

                     zeroCheck()

                     db.updateList(
                         Shopping_List_Data(
                             shopping_list_title_Value.text.toString(),
                             shopping_list_price_Value.text.toString(),
                             idTitle,
                             shopping_list_quantity_Value.text.toString(),
                             shopping_list_spinner_unit.selectedItem.toString(),
                             "stats",
                             listId,
                             imagePath
                         )
                     )
                     val cursor = db.getAllShoppingList(idTitle)
                     var total = 0.0f
                     if(cursor!!.moveToFirst())
                     {
                         total = cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat()
                         while(cursor.moveToNext())
                         {
                             total = total + ( cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat() )
                         }
                     }
                     db.updateMainShoppingTotal(idTitle, total.toString())
                 }
             }else if (status in getString(R.string.new_))
             {
                 if(!fieldEmptyCheck(shopping_list_title_Value))
                 {
                     zeroCheck()
                     Log.e("THIS IS PATH", imagePath)
                     db.insertList(
                         Shopping_List_Data(
                             shopping_list_title_Value.text.toString().trim(),
                             shopping_list_price_Value.text.toString(),
                             idTitle,
                             shopping_list_quantity_Value.text.toString(),
                             shopping_list_spinner_unit.selectedItem.toString(),
                             "0",
                             "0",
                              imagePath
                         )
                     )
                     val cursor = db.getAllShoppingList(idTitle)
                     var total = 0.0f
                     if(cursor!!.moveToFirst())
                     {
                         total = cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat()
                         while(cursor.moveToNext())
                         {
                             total = total + ( cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_PRICE)).toFloat() * cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_QUANTITY) ).toFloat() )
                         }
                     }
                     db.updateMainShoppingTotal(idTitle, total.toString())
                 }
             }
            db.close()
            finish()
        }
        shopping_list_button_cancel.setOnClickListener {
            functions.deleteImage(imagePath)
            finish()
        }
        shopping_list_title_Value.setOnTouchListener(View.OnTouchListener { v, event ->
            //val DRAWABLE_LEFT = 0
            //val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            //val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= shopping_list_title_Value.getRight() - shopping_list_title_Value.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    functions.hideKeyBoard(this)

                    val pop = PopupMenu(this, shopping_list_title_Value, Gravity.RIGHT)
                    pop.setOnMenuItemClickListener {
                            item: MenuItem? ->
                        when (item?.itemId)
                        {
                            R.id.menu_shopping_list_media_gallery ->{
                                choosePhotoFromGallary()
                            }
                            R.id.menu_shopping_list_media_camera ->{
                                takePhotoFromCamera()
                            }
                            else ->{
                            }
                        }
                        true

                    }
                    pop.inflate(R.menu.menu_shopping_list__media_popup)
                    try {
                        val field = PopupMenu::class.java.getDeclaredField("mPopup")
                        field.isAccessible = true
                        val mPopup = field.get(pop)
                        mPopup.javaClass
                            .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                            .invoke(mPopup, true)
                    }catch(e: Exception){
                        Log.e("MAIN", "ERROR SHOWING MENU ICONS",e)
                    }finally {
                        pop.show()
                    }

                    return@OnTouchListener true
                }
            }
            false
        })

        shopping_list_image.setOnClickListener {

            val pop = PopupMenu(this, shopping_list_image, Gravity.RIGHT)
            pop.setOnMenuItemClickListener {
                    item: MenuItem? ->
                when (item?.itemId)
                {
                    R.id.menu_shopping_list_image_viewer ->{
                        val tempInflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        var view = tempInflater.inflate(R.layout.membami_image_viewer_popupwindow_layout, null)
                        val imgV = view.findViewById<ImageView>(R.id.membami_image_viewer_main_image)


                        //val root = view.findViewById<EditText>(R.id.shopping_list_price_Value)
                        //val closeBtn = view.findViewById<ImageButton>(R.id.fyuzed_image_viewer_main_image_btn)
                        val popupWindow = PopupWindow(view,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            true)
                        popupWindow.animationStyle = R.style.MembaMi_IMage_viewer_Animation
                        popupWindow.showAtLocation(view.rootView, Gravity.CENTER,0,0)
                        popupWindow.isFocusable = true

                        Glide.with(applicationContext)
                            .load(imagePath)
                            .apply(
                                RequestOptions()
                                    .fitCenter()
                            )
                            .error(R.drawable.membami_camera_icon_weborange)
                            .into(imgV)
                        popupWindow.dimBehind()
                        popupWindow.update()


                        popupWindow.setOnDismissListener {
                        }

                    }
                    R.id.menu_shopping_list_image_viewer_delete ->{
                        tempimagePath = imagePath
                        toDelete = getString(R.string.delete)
                        imagePath = ""
                        shopping_list_image.visibility= View.GONE
                    }
                    else ->{
                    }
                }

                true

            }
            pop.inflate(R.menu.menu_shopping_list_image_viewer)
            try {
                val field = PopupMenu::class.java.getDeclaredField("mPopup")
                field.isAccessible = true
                val mPopup = field.get(pop)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            }catch(e: Exception){
                Log.e("MAIN", "ERROR SHOWING MENU ICONS",e)
            }finally {
                pop.show()
            }
        }

    }


    private fun fieldEmptyCheck(text1: EditText): Boolean{
        var result = false
        if(TextUtils.isEmpty(text1.text.toString()))
        {
            text1.error = getString(R.string.notEmpty)
            result = true
        }

        return result
    }
    private fun zeroCheck()
    {
        if(shopping_list_price_Value.text.toString()== "0" || TextUtils.isEmpty(shopping_list_price_Value.text.toString()))
            shopping_list_price_Value.text = Editable.Factory.getInstance().newEditable("1")

        if(shopping_list_quantity_Value.text.toString()== "0" || TextUtils.isEmpty(shopping_list_quantity_Value.text.toString()))
            shopping_list_quantity_Value.text = Editable.Factory.getInstance().newEditable("1")

        if(toDelete == getString(R.string.delete))
        {
            functions.deleteImage(tempimagePath)
            imagePath=""
        }


    }


    fun choosePhotoFromGallary() {
        if(imagePath != null)
        {
            val file = File(imagePath)
            if(file.exists()) {
                file.delete()
                Log.e("SONALI DELETED", "Deleted $imagePath")
            }
        }
        if(functions.getPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE ) && functions.getPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE ))
        {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(galleryIntent, GALLERY)
        }else
        {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE ),
                0)

        }

    }

    private fun takePhotoFromCamera() {

        if(imagePath != null)
        {
            val file = File(imagePath)
            if(file.exists()) {
                file.delete()
                Log.e("SONALI DELETED", "Deleted $imagePath")
            }
        }
        if (functions.getPermission(this, android.Manifest.permission.CAMERA) && functions.getPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE ))
        {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if(intent.resolveActivity(packageManager) != null) {
                var file : File? = null
                try{
                    file = createImageFile()
                }catch(e: IOException)
                {
                    Log.e("Error", "Happended")
                }
                if(file!=null)
                {
                    var photouri = FileProvider.getUriForFile(this, "${this.packageName}.provider", file)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photouri)
                    startActivityForResult(intent, CAMERA)
                }


            }
        }else
        {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE ),
                0)
        }

    }

    public override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        println(requestCode)
        println(resultCode)
        if (resultCode == -1)
        {
            when(requestCode)
            {
                GALLERY->{

                    functions.deleteImage(imagePath)
                    try {

                        var imageUri = data!!.data


                        var degress = functions.getDegrees(functions.getOrientation(this, imageUri))

                        var thumbnail  = MediaStore.Images.Media.getBitmap(contentResolver,imageUri)
                        thumbnail = functions.rotateImage(thumbnail, degress)
                        imagePath = functions.bitmapToFile(thumbnail, this).toString()


                        Glide.with(this)
                            .load(File(imagePath))
                            .apply(
                                RequestOptions()
                                    .circleCrop()
                            )
                            .error(R.drawable.membami_images_icon_vanilla)
                            .into(shopping_list_image)

                        shopping_list_image.visibility = View.VISIBLE
                    }catch (e: IOException) {
                        e.printStackTrace()
                        Log.e("ERROR ","Failed!")
                    }
                }
                CAMERA->{
                    try {
                        var bit = MediaStore.Images.Media.getBitmap(contentResolver,Uri.fromFile(File(imagePath)))
                        //var degrees = functions.getDegrees(functions.getOrientation(this,Uri.fromFile(File(imagePath))))
                        bit = functions.rotateImage(bit, 90)
                        var temy = functions.saveInternal(bit, this)
                        functions.deleteImage(imagePath)
                        imagePath = temy
                        Glide.with(applicationContext)
                            .load(File(imagePath))
                            .apply(
                                RequestOptions()
                                    .circleCrop()
                            )
                            .error(R.drawable.membami_camera_icon_weborange)
                            .into(shopping_list_image)
                        shopping_list_image.visibility = View.VISIBLE
                    }catch (e: IOException) {
                        e.printStackTrace()
                        Log.e("ERROR ","Failed!")
                    }

                }
            }
        } else if ( resultCode == Activity.RESULT_CANCELED)
        {
            functions.deleteImage(imagePath)
        }
    }

    fun createImageFile() : File {
     var timeStamp = Calendar.getInstance().timeInMillis.toString()
     var imageFileName = "IMG_" + timeStamp + "_";
     var storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    var image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
         )

    imagePath = image.getAbsolutePath()
    return image
}
    fun PopupWindow.dimBehind() {
        val container = contentView.rootView
        val context = contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.6f
        wm.updateViewLayout(container, p)
    }

    /**
    fun  getRealPathFromURI(contentUri:Uri): String {
        var proj = arrayOf( MediaStore.Images.Media.DATA )
        var cursor = managedQuery(contentUri, proj, null, null, null)
        var column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index)
    }

    /**fun  rotateImageIfRequired( img: Bitmap,  selectedImage :Uri) :String {

    var  ei =  ExifInterface(selectedImage.getPath());
    var orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        when(orientation) {
            ExifInterface.ORIENTATION_ROTATE_90->
                return rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180->
                return rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270->
                return rotateImage(img, 270)
            else->
                return img
        }
    }*/


    private fun getRightAngleImage(photoPath: String): String {

        try {
            val ei = ExifInterface(photoPath)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            var degree = 0

            when (orientation) {
                ExifInterface.ORIENTATION_NORMAL -> degree = 0
                ExifInterface.ORIENTATION_ROTATE_90 -> degree = 90
                ExifInterface.ORIENTATION_ROTATE_180 -> degree = 180
                ExifInterface.ORIENTATION_ROTATE_270 -> degree = 270
                ExifInterface.ORIENTATION_UNDEFINED -> degree = 0
                else -> degree = 90
            }

            return rotateImage(degree.toFloat(), photoPath)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return photoPath
    }

    private fun rotateImage(degree: Float, imagePath: String): String {

        if (degree <= 0) {
            return imagePath
        }
        try {
            var b = BitmapFactory.decodeFile(imagePath)

            val matrix = Matrix()
            if (b.width > b.height) {
                matrix.setRotate(degree)
                b = Bitmap.createBitmap(
                    b, 0, 0, b.width, b.height,
                    matrix, true
                )
            }

            val fOut = FileOutputStream(imagePath)
            val imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1)
            val imageType = imageName.substring(imageName.lastIndexOf(".") + 1)

            val out = FileOutputStream(imagePath)
            if (imageType.equals("png", ignoreCase = true)) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out)
            } else if (imageType.equals("jpeg", ignoreCase = true) || imageType.equals("jpg", ignoreCase = true)) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out)
            }
            fOut.flush()
            fOut.close()

            b.recycle()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return imagePath
    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }
    private fun setCapturedImage(imagePath: String) {
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg params: Void): String {
                try {
                    return getRightAngleImage(imagePath)
                } catch (e: Throwable) {
                    e.printStackTrace()
                }

                return imagePath
            }

            override fun onPostExecute(imagePath: String) {
                super.onPostExecute(imagePath)
                shopping_list_image?.setImageBitmap(decodeFile(imagePath))
            }
        }.execute()
    }

    fun decodeFile(path: String): Bitmap? {
        try {
            // Decode deal_image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeFile(path, o)
            // The new size we want to scale to
            val REQUIRED_SIZE = 1024

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2
            // Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeFile(path, o2)
        } catch (e: Throwable) {
            e.printStackTrace()
        }

        return null
    }

    fun getAbsolutePath(uri: Uri): String? {
        if (Build.VERSION.SDK_INT >= 19) {
            var id = ""
            if (uri.getLastPathSegment().split(":").size > 1)
                id = uri.getLastPathSegment().split(":")[1]
            else if (uri.getLastPathSegment().split(":").size > 0)
                id = uri.getLastPathSegment().split(":")[0]
            if (id.length > 0) {
                val imageColumns = arrayOf(MediaStore.Images.Media.DATA)
                val imageOrderBy: String? = null
                val tempUri = getUri()
                val imageCursor = getContentResolver().query(
                    tempUri,
                    imageColumns,
                    MediaStore.Images.Media._ID + "=" + id,
                    null,
                    imageOrderBy
                )
                return if (imageCursor.moveToFirst()) {
                    imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA))
                } else {
                    null
                }
            } else {
                return null
            }
        } else {
            val projection = arrayOf(MediaStore.MediaColumns.DATA)
            val cursor = getContentResolver().query(uri, projection, null, null, null)
            if (cursor != null) {
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                cursor!!.moveToFirst()
                return cursor!!.getString(column_index)
            } else
                return null
        }

    }

    private fun getUri(): Uri {
        val state = Environment.getExternalStorageState()
        return if (!state.equals(
                Environment.MEDIA_MOUNTED,
                ignoreCase = true
            )
        ) MediaStore.Images.Media.INTERNAL_CONTENT_URI else MediaStore.Images.Media.EXTERNAL_CONTENT_URI

    }

    fun setImageUri(): Uri {
        val imgUri: Uri
        val state = Environment.getExternalStorageState()
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            val file = File(
                "${Environment.getExternalStorageDirectory()} /DCIM/",
                getString(R.string.app_name) + Calendar.getInstance().getTimeInMillis() + ".png"
            )
            imgUri = Uri.fromFile(file)
            imgPath = file.getAbsolutePath()
        } else {
            val file =
                File(getFilesDir(), getString(R.string.app_name) + Calendar.getInstance().getTimeInMillis() + ".png")
            imgUri = Uri.fromFile(file)
            this.imgPath = file.getAbsolutePath()
        }
        return imgUri
    }

    fun getImagePath(): String {
        return imgPath!!
    }
    */






}
