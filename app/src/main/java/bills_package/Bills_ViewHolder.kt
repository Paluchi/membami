package bills_package


import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.sonalipaluchi.membami.R

class Bills_ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

    var billsid_viewholder :TextView? = null
    var billstitle_viewholder : TextView? = null
    var billsamount_viewholder : TextView? = null
    var billsrepeat_viewholder : TextView? = null
    var billsbalance_viewholder : TextView? = null
    var billsMainLayout : RelativeLayout? = null
    //var billsoptions_viewholder : Button? =null


    init {
        billsid_viewholder  = itemView.findViewById(R.id.bills_id_textView)
        billstitle_viewholder = itemView.findViewById<TextView>(R.id.bill_Title)
        billsamount_viewholder = itemView.findViewById<TextView>(R.id.bill_due_Amount_Value)
        billsrepeat_viewholder = itemView.findViewById(R.id.bill_repeat_textView)
        billsbalance_viewholder = itemView.findViewById(R.id.bill_balance_created_textView)
        billsMainLayout = itemView.findViewById(R.id.bills_cardlayoutMainBIlls)
        //billsoptions_viewholder = itemView.findViewById<Button>(R.id.buttonOptions)
    }
   /** override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {

        menu?.add(this.getAdapterPosition(), 121, 0, "DELETE")
        menu?.add(this.getAdapterPosition(), 121, 0, "TET")
    }*/

}