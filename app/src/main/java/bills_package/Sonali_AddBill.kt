package bills_package

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.text.Editable
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.activity_sonali_add_bill.*


class Sonali_AddBill : AppCompatActivity() {


    //var sonali_priority = arrayOf("URGENT", "HIGH", "MEDIUM", "LOW")
  //  var cal = Calendar.getInstance()
    var sentInfo : String? =null
    val functions = General_Functions()
    var oldTitle = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sonali_add_bill)
        sentInfo = intent.getStringExtra("extraData")
        sonaliListeners()
        updateBill(sentInfo.toString())

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_bill, menu)



        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.menu_add_bill_save ->{

                if(!fieldEmptyCheck()){
                    if(TextUtils.isEmpty(amountBills_ET.text.toString()) || amountBills_ET.text.toString() =="0")
                    {
                        amountBills_ET.text = Editable.Factory.getInstance().newEditable("1")
                    }
                    if(sentInfo == getString(R.string.new_))
                    {
                        val db = Retain_Information(this, null)
                        db.addBill(
                            Bills_Data(0,add_bill_EditText.text.toString(),
                                functions.getFloat(amountBills_ET.text.toString()),
                                dueperBillsDDL.selectedItem.toString(),
                                "0",
                                colorBillsET.text.toString()
                            )
                        )
                        db.createPayment(db.getBillID_ForTItle())
                        db.close()
                    }else if(sentInfo != getString(R.string.new_) )
                    {
                        val db = Retain_Information(this, null)
                        //constructor(id :Int, a : String, amt :Float, due: String, createdDate :String)
                        db.updateBill(
                            Bills_Data(sentInfo!!.toInt(),add_bill_EditText.text.toString(),
                                functions.getFloat(amountBills_ET.text.toString()),
                                dueperBillsDDL.selectedItem.toString(),
                                "0",
                                colorBillsET.text.toString()
                            )
                        )
                        db.close()
                    }
                    finish()
                }
            }
            R.id.menu_add_bill_cancel ->{
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
    private fun sonaliListeners()
    {
        var due_per = functions.billDuePer(this)

        val spinner = this.findViewById<Spinner>(R.id.dueperBillsDDL)
        val arrayAdapter= ArrayAdapter(this, R.layout.spinner_row_modifier ,due_per)
        spinner?.adapter = arrayAdapter
    }

    private fun fieldEmptyCheck(): Boolean{
        var result = false
        if(TextUtils.isEmpty(add_bill_EditText.text.toString()))
        {
            add_bill_EditText.error = getString(R.string.notEmpty)
            result = true
        }
        /**if(TextUtils.isEmpty(amountBills_ET.text.toString()))
        {
            amountBills_ET.error = getString(R.string.notEmpty)
            result = true
        }*/

        return result
    }



    private fun updateBill(id : String)
    {
        var due_per = functions.billDuePer(this)

        if (id !=  getString(R.string.new_))
        {
            val db = Retain_Information(this, null)
            val cursor = db.searchBill(sentInfo!!.toInt())
            if(cursor!!.moveToFirst())
            {
                add_bill_EditText.text = Editable.Factory.getInstance().newEditable(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_TITLE))
                )
                oldTitle = cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_TITLE))

                amountBills_ET.text = Editable.Factory.getInstance().newEditable(functions.heavyFunction(cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_AMT))))

                dueperBillsDDL.setSelection(due_per.indexOf( cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_REAPEAT)) ))

                colorBillsET.text = Editable.Factory.getInstance().newEditable(
                    cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_COLOR))
                )
                supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>Edit Bill </font>",HtmlCompat.FROM_HTML_MODE_LEGACY)
            }


        }else{
            supportActionBar?.title =HtmlCompat.fromHtml("<font color='#8B4513'>New Bill </font>",HtmlCompat.FROM_HTML_MODE_LEGACY)
        }

    }




}
