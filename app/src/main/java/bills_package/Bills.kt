package bills_package

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.fragment_bills.*


class Bills : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var billsAdapter : BillsRecyclerAdapter? = null
    var sonaliList = ArrayList<Bills_Data>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bills, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializer()
    }
    override fun onResume() {
        super.onResume()
        initializer()
    }

    fun populateList(): ArrayList<Bills_Data>
    {
        var list = ArrayList<Bills_Data>()
        val dbHandler = Retain_Information(view!!.context, null)
        val cursor = dbHandler.getAllBills()

        if(cursor!!.moveToFirst())
        {
           list.add(
               Bills_Data(cursor.getInt(cursor.getColumnIndex(Retain_Information.BILL_ID)),
                   cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_TITLE)),
                   cursor.getFloat(cursor.getColumnIndex(Retain_Information.BILL_AMT)),
                   cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_REAPEAT)),
                   cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_BALANCE)),
                   cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_COLOR))

               )
           )

            while(cursor.moveToNext())
            {
                list.add(
                    Bills_Data(cursor.getInt(cursor.getColumnIndex(Retain_Information.BILL_ID)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_TITLE)),
                        cursor.getFloat(cursor.getColumnIndex(Retain_Information.BILL_AMT)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_REAPEAT)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_BALANCE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_COLOR))
                    )
                )
            }
        }
        dbHandler.close()
        cursor.close()


        return list
    }

    fun initializer()
    {
        val db = Retain_Information(context!!, null)
        val cursor = db.getSettings()
        if(cursor.moveToFirst())
        {
            GLB_CUR = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_CURRENCY))
            GLB_UNIT = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_UNIT))
        }
        cursor.close()
        db.close()




        recyclerView = view?.findViewById<RecyclerView>(R.id.bills_recycler_view)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        //recyclerView!!.layoutManager = LinearLayoutManager(view!!.context, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        sonaliList = populateList()
        billsAdapter = BillsRecyclerAdapter(view!!.context, sonaliList)
        recyclerView!!.adapter = billsAdapter
       // recyclerView!!.adapter!!.notifyDataSetChanged()

        checkIfEmptyList(sonaliList)

        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0 && !bills_fab.isShown())
                    bills_fab.show()
                else if (dy > 0 && bills_fab.isShown())
                    bills_fab.hide()
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

        billslisteners()

    }

    private fun checkIfEmptyList(list : ArrayList<Bills_Data>)
    {
        if(list.size <= 0)
        {
            placeholderImageBills.visibility = View.VISIBLE
            bills_recycler_view.visibility =View.GONE
            bills_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!,R.color.sonaliWhite))
        }else
        {
            placeholderImageBills.visibility = View.GONE
            bills_recycler_view.visibility =View.VISIBLE
            bills_recycler_frame.setBackgroundColor(ContextCompat.getColor(context!!,R.color.sonaliGrey))
        }

    }

    fun billslisteners()
    {
        bills_fab.setOnClickListener {
            val intent = Intent(view!!.context, Sonali_AddBill::class.java)
            intent.putExtra("extraData", getString(R.string.new_))
            startActivity(intent)
        }
    }
}