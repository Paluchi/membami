package bills_package.detailed_bills

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.sonalipaluchi.membami.R


class Bills_DetailsViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    //var bills_id_viewholder :TextView ? = null
    var bill_paid_amount_viewholder : TextView? = null
    var bill_amount_viewholder: TextView? = null
    var bill_balance_viewholder : TextView? = null
    var bill_occurence_viewholder : TextView? = null
    var bill_date_vielholder : TextView? = null

    init {
        bill_paid_amount_viewholder = itemView.findViewById(R.id.paidAmount)
        bill_amount_viewholder = itemView.findViewById<TextView>(R.id.bill_amt)
        bill_balance_viewholder = itemView.findViewById<TextView>(R.id.paymentBalanceValue)
        bill_occurence_viewholder = itemView.findViewById<TextView>(R.id.bill_occurenceValue)
        bill_date_vielholder    = itemView.findViewById(R.id.paidDateRecyclerViewValue)

    }

}