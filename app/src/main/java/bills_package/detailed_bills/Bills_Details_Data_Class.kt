package bills_package.detailed_bills

class Bills_Details_Data_Class
{
    internal var bills_details_data_class_paidAmount = ""
    internal var bills_details_data_class_billAmt = ""
    internal var bills_details_data_class_occurrence   = ""
    internal var bills_details_data_class_paymentBalance = ""
    internal var bills_details_data_class_paidDateRCVValue =""

    constructor(amount: String, billAmt:String, occrrence:String, paybal :String, payDate : String)
    {
        this.bills_details_data_class_paidAmount=amount
        this.bills_details_data_class_billAmt = billAmt
        this.bills_details_data_class_occurrence = occrrence
        this.bills_details_data_class_paymentBalance = paybal
        this.bills_details_data_class_paidDateRCVValue = payDate
    }
}
