package bills_package.detailed_bills

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.text.HtmlCompat
import android.text.Editable
import android.text.TextUtils
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.GlobalVariable
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import kotlinx.android.synthetic.main.activity_make__payment.*

class Make_Payment : AppCompatActivity() {
    var repeat = ""
    var title = ""
    var billamt=""
    var bill_ID = ""
    val function = General_Functions()
    val glb     = GlobalVariable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make__payment)
        repeat = intent.getStringExtra("paymentOccurrence")
        title = intent.getStringExtra("paymentTitle")
        billamt = intent.getStringExtra("paymentAmtConst")
        bill_ID = intent.getStringExtra("paymentID")


        supportActionBar?.title = (HtmlCompat.fromHtml("<font color='#8B4513'>${title.capitalize()} </font>", HtmlCompat.FROM_HTML_MODE_LEGACY))
        makePaymentTitle.text = title
        makePayment_billAmountValue.text = Editable.Factory.getInstance().newEditable(function.formatMOney(billamt.toDouble(),
            GlobalVariable.GLB_CUR,
            GlobalVariable.GLB_UNIT
        ))
        listeners()
    }

    fun listeners()
    {
        makePaymentButton.setOnClickListener{
            if(!fieldEmptyCheck()) {

                val db = Retain_Information(this, null)
                val paidAmt = function.getFloat(makePayment_AmountPayValue.text.toString()).toString()
                val billAMT = function.getFloat(makePayment_billAmountValue.text.toString()).toString()
                db.savePayment(
                    Bills_Details_Data_Class(
                        paidAmt,
                        billAMT,
                        repeat,
                        db.calculateRemaining(paidAmt.toFloat(), billAMT.toFloat(),bill_ID).toString(),
                        function.getDate()
                    ),
                    bill_ID
                )
                val balance = db.getLastOutstanding(bill_ID)
                db.updateBillBalance(bill_ID,balance.toString())
                db.close()
                finish()
                //calculateRemaining( paidAmt:Float, dueAmt:Float, name:String):Float
            }
        }
        /**makePayment_AmountPayValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                var temp = makePayment_AmountPayValue.text.toString()
                var tofield = function.formatMOney(temp.toDouble())
                makePayment_AmountPayValue.text = Editable.Factory.getInstance().newEditable(tofield)

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })*/

    }

    private fun fieldEmptyCheck(): Boolean{
        var result = false
        if(TextUtils.isEmpty(makePayment_AmountPayValue.text.toString()))
        {
            makePayment_AmountPayValue.error = getString(R.string.notEmpty)
            result = true
        }

        return result
    }


}
