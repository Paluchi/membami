package bills_package.detailed_bills

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.text.HtmlCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.sonalipaluchi.membami.R
import com.sonalipaluchi.membami.Retain_Information
import android.view.View
import kotlinx.android.synthetic.main.activity_bills__details__main.*


class Bills_Details_Main : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var billsAdapter : Bills_DetailsRecyclerAdapter? = null
    var sonaliList = ArrayList<Bills_Details_Data_Class>()
    var title = ""
    var ID = ""
    val mainTitle = " Payments"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bills__details__main)
        //sent_data = intent.getStringExtra("Bill_Details")
        title = intent.getStringExtra("Bills_Details_Title")
        ID = intent.getStringExtra("Bills_Details_ID")
        supportActionBar?.title =  HtmlCompat.fromHtml("<font color='#8B4513'>${title.capitalize()}$mainTitle </font>", HtmlCompat.FROM_HTML_MODE_LEGACY )
        initializer()

    }


    override fun onRestart() {
        super.onRestart()
        initializer()
    }

    fun populateList(): ArrayList<Bills_Details_Data_Class>
    {
        var list = ArrayList<Bills_Details_Data_Class>()
        val dbHandler = Retain_Information(applicationContext, null)
        val cursor = dbHandler.getAllPayment(ID)

        if(cursor.moveToFirst())
        {
            list.add(
                Bills_Details_Data_Class(
                cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_AMT)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DUE_AMT)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_OCCURRENCE)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_OUTSTANDING)),
                    cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DATE))
                )

            )
            println( cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DATE)))
            println( cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DATE)))
            println( cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DATE)))
            while(cursor.moveToNext())
            {
                list.add(
                    Bills_Details_Data_Class(
                        cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_AMT)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DUE_AMT)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_OCCURRENCE)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_OUTSTANDING)),
                        cursor.getString(cursor.getColumnIndex(Retain_Information.PAID_DATE))
                    )
                )
            }
        }
        dbHandler.close()
       // cursor.close()

        return list
    }

    private fun initializer()
    {
        recyclerView = findViewById<RecyclerView>(R.id.bills_details_recycler_view)
        //recyclerView!!.layoutManager = GridLayoutManager(view!!.context,2)
        recyclerView!!.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        //recyclerView!!.addItemDecoration(DividerItemDecoration(recyclerView!!.getContext(), DividerItemDecoration.VERTICAL))
        sonaliList = populateList()
        billsAdapter = Bills_DetailsRecyclerAdapter(applicationContext, sonaliList)
        recyclerView!!.adapter = billsAdapter
        checkIfEmptyList(sonaliList)




    }
    private fun checkIfEmptyList(list : ArrayList<Bills_Details_Data_Class>)
    {
        if(list.size <= 0)
        {
            placeholderImageBillsDetails.visibility = View.VISIBLE
            bills_details_recycler_view.visibility =View.GONE
            main_bills_details_frame_layout.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.sonaliWhite))
        }else
        {
            placeholderImageBillsDetails.visibility = View.GONE
            bills_details_recycler_view.visibility =View.VISIBLE
            main_bills_details_frame_layout.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.sonaliGrey))
        }

    }



}
