package bills_package.detailed_bills

class Get_Bills_Database_Class
{
    internal var bills_database_class_id        = 0
    internal var bills_database_class_title     = ""
    internal var bills_database_class_amount    = 0.0f
    internal var bills_database_class_repeat    = ""
    internal var bills_database_class_createdate = ""

    constructor( id:Int, title: String, Amt:Float, repeat:String, credate:String)
    {
        this.bills_database_class_id            = id
        this.bills_database_class_title         = title
        this.bills_database_class_amount        = Amt
        this.bills_database_class_repeat        = repeat
        this.bills_database_class_createdate    = credate
    }
}