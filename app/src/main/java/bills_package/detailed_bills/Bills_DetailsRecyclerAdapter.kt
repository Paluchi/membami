package bills_package.detailed_bills


import android.support.v7.widget.RecyclerView
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.ViewGroup
import com.sonalipaluchi.membami.R
import android.widget.*
import com.sonalipaluchi.membami.General_Functions
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT


class Bills_DetailsRecyclerAdapter(ctx: Context, private val imageModelArrayList: ArrayList<Bills_Details_Data_Class>) : RecyclerView.Adapter<Bills_DetailsViewHolder>() {

    private val inflater: LayoutInflater
    private var relativeLayout:RelativeLayout? = null
    var functions = General_Functions()


    init {

        inflater = LayoutInflater.from(ctx)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Bills_DetailsViewHolder {

        val view = inflater.inflate(R.layout.activity_bills_details_main_row_data, parent, false)
        relativeLayout?.rootView?.findViewById<RelativeLayout>(R.id.main_bills_details_frame_layout)

        return Bills_DetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: Bills_DetailsViewHolder, position: Int) {
        // holder?.radio_image?.setImageBitmap(imageModelArrayList[position].image)
        holder.bill_paid_amount_viewholder!!.text = functions.formatMOney(imageModelArrayList[position].bills_details_data_class_paidAmount.toDouble(),
            GLB_CUR, GLB_UNIT
        )
        holder.bill_amount_viewholder!!.text = functions.formatMOney(imageModelArrayList[position].bills_details_data_class_billAmt.toDouble(),
            GLB_CUR, GLB_UNIT
        )
        holder.bill_occurence_viewholder!!.text = imageModelArrayList[position].bills_details_data_class_occurrence
        holder.bill_balance_viewholder!!.text = functions.formatMOney(imageModelArrayList[position].bills_details_data_class_paymentBalance.toDouble(),
            GLB_CUR, GLB_UNIT
        )
        holder.bill_date_vielholder!!.text = imageModelArrayList[position].bills_details_data_class_paidDateRCVValue


        /** println("<<<<")
        println("IT CONTAINS -----")
        println(imageModelArrayList[position].bills_details_data_class_paymentBalance)
        println(">>>>>>>")
         */

        if(imageModelArrayList[position].bills_details_data_class_paymentBalance.contains("-",true))
        {
            holder.bill_balance_viewholder!!.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.sonaliRed))
        }else
        {
            holder.bill_balance_viewholder!!.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.sonaliGreen))
        }

    }

    override fun getItemCount(): Int {
        return imageModelArrayList.size
    }



}