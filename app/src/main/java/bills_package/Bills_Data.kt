package bills_package

class Bills_Data
{
    internal var billid : Int = 0
    internal var billTitle: String =""
    internal var billAmt: Float = 0.02f
    internal var billdue: String = ""
    internal var billbalance : String = ""
    internal var billColor : String = ""
    constructor()
    {
        this.billid = 0
        this.billTitle = ""
        this.billAmt = 0.00f
        this.billdue = ""
        this.billbalance = ""
        this.billColor = ""
    }

   constructor(id :Int, a : String, amt :Float, due: String, balance :String, billColor :String)
    {
        this.billid =id
        this.billTitle = a
        this.billAmt = amt
        this.billdue = due
        this.billbalance = balance
        this.billColor = billColor

    }


    fun getBillTitle() :String {return this.billTitle}
    fun getBillAmt() :Float{return this.billAmt}
    fun getBillDue() :String{return this.billdue}
    fun getBillBalance(): String{return this.billbalance}


}