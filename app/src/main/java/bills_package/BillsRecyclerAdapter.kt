package bills_package


import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import android.view.*
import android.widget.*
import bills_package.detailed_bills.Bills_Details_Main
import bills_package.detailed_bills.Make_Payment
import com.sonalipaluchi.membami.*
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT


class BillsRecyclerAdapter(ctx: Context, private val imageModelArrayList: ArrayList<Bills_Data>) : RecyclerView.Adapter<Bills_ViewHolder>() {

    private val inflater: LayoutInflater
    private var relativeLayout:RelativeLayout? = null
    val general_Functions = General_Functions()

    init {

        inflater = LayoutInflater.from(ctx)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Bills_ViewHolder {
        val view = inflater.inflate(R.layout.bills_recycler_row_layout, parent, false)
        relativeLayout?.rootView?.findViewById<RelativeLayout>(R.id.bills_recycler_frame)

        return Bills_ViewHolder(view)
    }


    @SuppressLint("RestrictedApi")
    override fun onBindViewHolder(holder: Bills_ViewHolder, position: Int) {
        holder.billsid_viewholder!!.text = imageModelArrayList[position].billid.toString()
        holder.billstitle_viewholder!!.text = imageModelArrayList[position].billTitle.capitalize()

        holder.billsamount_viewholder!!.text = general_Functions.formatMOney(imageModelArrayList[position].billAmt.toString().toDouble(), GLB_CUR,
            GLB_UNIT)
        holder.billsrepeat_viewholder!!.text = imageModelArrayList[position].billdue
        holder.billsbalance_viewholder!!.text = general_Functions.formatMOney(imageModelArrayList[position].billbalance.toDouble(), GLB_CUR,
            GLB_UNIT)
        holder.billsMainLayout?.setBackgroundColor(ContextCompat.getColor(holder.itemView.context,general_Functions.getRandomColor()))

        if(imageModelArrayList[position].billbalance.contains("-",true))
        {
            holder.billsbalance_viewholder!!.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.sonaliRed))
        }else
        {
            holder.billsbalance_viewholder!!.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.sonaliGreen))
            holder.billsbalance_viewholder!!.text = "+ ${holder.billsbalance_viewholder!!.text}"
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            val pop = PopupMenu(holder.itemView.context, holder.itemView, Gravity.RIGHT)

            pop.setOnMenuItemClickListener {
                item: MenuItem? ->
                when (item?.itemId)
                {
                    R.id.menu_view_all_bill ->
                    {
                        var intent =Intent(holder.itemView.context, Bills_Details_Main::class.java)
                        //intent.putExtra("Bill_Details", (imageModelArrayList[position].bills_recycler_data_class_title))
                        intent.putExtra("Bills_Details_Title", imageModelArrayList[position].billTitle)
                        intent.putExtra("Bills_Details_ID", imageModelArrayList[position].billid.toString())
                        startActivity(holder.itemView.context, intent,null) /***/
                        true
                    }
                    R.id.menu_pay_bill ->{
                        val intent = Intent(holder.itemView.context, Make_Payment::class.java)
                        intent.putExtra("paymentAmtConst", imageModelArrayList[position].billAmt.toString())
                        intent.putExtra("paymentOccurrence", imageModelArrayList[position].billdue)
                        intent.putExtra("paymentTitle", imageModelArrayList[position].billTitle)
                        intent.putExtra("paymentID", imageModelArrayList[position].billid.toString())
                        startActivity(holder.itemView.context, intent,null)

                            true
                    }
                    R.id.menu_edit_bill ->{
                        val intent = Intent(holder.itemView.context, Sonali_AddBill::class.java)
                        intent.putExtra("extraData", imageModelArrayList[position].billid.toString())
                        startActivity(holder.itemView.context, intent, null)

                        true
                    }
                    R.id.menu_delete_bill ->{

                        val intent = Intent(holder.itemView.context, MainDelete::class.java)
                        intent.putExtra("DELETE_ID", imageModelArrayList[position].billid.toString())
                        intent.putExtra("DELETE_TYPE", holder.itemView.context.getString(R.string.tab_bills))
                        startActivity(holder.itemView.context, intent, null)



                        true
                    }else ->{false}
                }

            }
            pop.inflate(R.menu.menu_bills_popup)
            try {
                val field = PopupMenu::class.java.getDeclaredField("mPopup")
                field.isAccessible = true
                val mPopup = field.get(pop)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            }catch(e: Exception){
                Log.e("MAIN", "ERROR SHOWING MENU ICONS",e)
            }finally {
                pop.show()
            }

        })

    }

    override fun getItemCount(): Int {
        return imageModelArrayList.size
    }



}