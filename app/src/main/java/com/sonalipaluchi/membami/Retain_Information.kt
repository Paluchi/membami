package com.sonalipaluchi.membami

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import bills_package.Bills_Data
import bills_package.detailed_bills.Bills_Details_Data_Class
import notes_package.NotesDetails
import shopping_package.shopping_details_view.shopping_details_listRecycler_package.Shopping_List_Data
import shopping_package.shopping_recycler_package.Shopping_Main_Data

class Retain_Information(context: Context, factory: SQLiteDatabase.CursorFactory?): SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    var dbName = "MembaMiDatabase"
    var notesTable = "Notes"
    var noteID = "ID"
    var noteTitle = "Title"
    var noteDesc = "Description"
    var dbVersion = 1
    var payHeading = "Pay_"
    val shopList = "ShoppingList_"
    val functions = General_Functions()

    override fun onCreate(db: SQLiteDatabase?) {
        //val sqlCreateTable = "CREATE TABLE IF NOT EXISTS $TABLE_NAME ( $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $TITLE TEXT, $DESCRIPTION TEXT, $MOD_DATE TEXT);"
        val temp =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME ( $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $TITLE TEXT, $DESCRIPTION TEXT, $MOD_DATE TEXT);"
        val billsTable =
            "CREATE TABLE IF NOT EXISTS $BILLTABLE ($BILL_ID INTEGER PRIMARY KEY AUTOINCREMENT, $BILL_TITLE TEXT, $BILL_AMT TEXT, $BILL_REAPEAT TEXT, $BILL_BALANCE TEXT, $BILL_COLOR TEXT);"

        val shoppingTable =
            "CREATE TABLE IF NOT EXISTS $SHOPPING_TABLE ( $SHOPPING_ID INTEGER PRIMARY KEY AUTOINCREMENT, $SHOPPING_TITLE TEXT, $SHOPPING_DATE TEXT, $SHOPPING_ITEM_DONE TEXT, $SHOPPING_ITEM_TOTAL TEXT, $SHOPPING_TOTAL TEXT)"

        val settingstable =
            "CREATE TABLE IF NOT EXISTS $SETTINGS_TABLE ( $SETTINGS_ID INTEGER PRIMARY KEY AUTOINCREMENT, $SETTINGS_COUNTRY TEXT, $SETTINGS_CURRENCY TEXT, $SETTINGS_UNIT TEXT);"


        db!!.execSQL(temp)
        db.execSQL(billsTable)
        db.execSQL(shoppingTable)
        db.execSQL(settingstable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun insertNote(note: NotesDetails) {
        val values = ContentValues()
        values.put(noteTitle, note.getNotesTitle())
        values.put(noteDesc, note.getNotesDesc())
        values.put(MOD_DATE, note.getNotes_Mod_Note())
        val db = this.writableDatabase
        db.insert(notesTable, null, values)
        db.close()
        println("<<<<<<<<<<inserted>>>>>>>>>>>")
    }

    fun getAllNotes(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME ORDER BY $COLUMN_ID DESC", null)
    }

    fun searchNote(id: Int): Cursor? {
        val db = this.readableDatabase
        // println("THIS IS THE CURSOR\n\n")
        //println(db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $COLUMN_ID=$id", null))
        return db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $COLUMN_ID=$id", null)

    }

    fun update(note: NotesDetails, id: Int) {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(TITLE, note.getNotesTitle())
        values.put(DESCRIPTION, note.getNotesDesc())
        values.put(MOD_DATE, note.getNotes_Mod_Note())
        db.update(TABLE_NAME, values, "$COLUMN_ID = $id", null)
        db.close()
    }

    fun deleteNote(id: Int): Boolean {
        val db = this.readableDatabase
        return db.delete(TABLE_NAME, "$COLUMN_ID = $id", null) > 0
    }

    /**TODO THIS IS THE BEGINNING OF BILLS DATABASE FUNCTIONS*/
    fun addBill(bills_Data: Bills_Data) {

        val db = this.readableDatabase
        val values = ContentValues()
        values.put(BILL_TITLE, bills_Data.billTitle)
        values.put(BILL_AMT, bills_Data.billAmt)
        values.put(BILL_REAPEAT, bills_Data.billdue)
        values.put(BILL_BALANCE, bills_Data.billbalance)
        values.put(BILL_COLOR, bills_Data.billColor)
        db.insert(BILLTABLE, null, values)
        db.close()
        //createBill(getBillID(bills_Data) + BILL_TITLE)


    }

    fun getBillID_ForTItle(): String {
        val db = this.readableDatabase
        var ans = ""
        val cursor = db.rawQuery("SELECT $BILL_ID FROM $BILLTABLE ORDER BY $BILL_ID DESC LIMIT 1", null)
        if (cursor.moveToFirst()) {
            ans = cursor.getString(cursor.getColumnIndex(BILL_ID))
        }
        return ans
    }

    fun getAllBills(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $BILLTABLE", null)
    }

    fun searchBill(id: Int): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $BILLTABLE WHERE $BILL_ID=$id", null)
    }

    fun updateBillBalance(id: String, balance: String) {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(BILL_BALANCE, balance)
        db.update(BILLTABLE, values, "$BILL_ID = $id", null)

    }

    fun updateBill(bills_Data: Bills_Data) {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(BILL_TITLE, bills_Data.billTitle)
        values.put(BILL_AMT, bills_Data.billAmt)
        values.put(BILL_REAPEAT, bills_Data.billdue)
        values.put(BILL_COLOR, bills_Data.billColor)
        db.update(BILLTABLE, values, "$BILL_ID = ${bills_Data.billid}", null)
    }

    fun deleteBill(id: Int) {
        val db = this.readableDatabase
        db.delete(BILLTABLE, "$BILL_ID = $id", null)
        db.close()
    }


    /**TODO PAYMENTS MADE DATABASES*/
    fun createPayment(name: String) {
        val db = this.readableDatabase
        val paysql =
            "CREATE TABLE IF NOT EXISTS $payHeading$name ($PAID_ID INTEGER PRIMARY KEY AUTOINCREMENT, $PAID_AMT TEXT, $PAID_DUE_AMT TEXT, $PAID_OCCURRENCE TEXT, $PAID_OUTSTANDING TEXT, $PAID_DATE TEXT);"
        db!!.execSQL(paysql)

        // savePayment(pay)

    }

    /**fun updatePaymentTable(newtitle:String, oldtitle : String)
    {
    val db = this.readableDatabase
    db.execSQL("ALTER TABLE $payHeading$oldtitle RENAME TO $newtitle")

    db.close()
    }*/
    fun savePayment(pay_Database_Class: Bills_Details_Data_Class, title: String) {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(PAID_AMT, pay_Database_Class.bills_details_data_class_paidAmount)
        values.put(PAID_DUE_AMT, pay_Database_Class.bills_details_data_class_billAmt)
        values.put(PAID_OCCURRENCE, pay_Database_Class.bills_details_data_class_occurrence)
        values.put(PAID_OUTSTANDING, pay_Database_Class.bills_details_data_class_paymentBalance)
        values.put(PAID_DATE, pay_Database_Class.bills_details_data_class_paidDateRCVValue)
        db.insert("$payHeading$title", null, values)
        db.close()
    }

    fun getAllPayment(name: String): Cursor {
        val db = this.readableDatabase
        // deletePayments(name)
        return db.rawQuery("SELECT * FROM $payHeading$name ORDER BY $PAID_ID DESC", null)
    }

    fun getLastOutstanding(name: String): Float {
        var ans = 0.0f
        val db = this.readableDatabase
        try {
            val temp =
                db.rawQuery("SELECT $PAID_OUTSTANDING FROM $payHeading$name ORDER BY $PAID_ID DESC LIMIT 1", null)
            if (temp.moveToFirst()) {
                ans = temp.getFloat(temp.getColumnIndex(PAID_OUTSTANDING))

            }
        } catch (sql: SQLException) {
            ans = 0.0f
        }
        return ans
    }

    fun calculateRemaining(paidAmt: Float, dueAmt: Float, name: String): Float {
        val ans = paidAmt - dueAmt
        return ans + getLastOutstanding("$payHeading$name")
    }

    fun deletePayments(title: String)
    {
        val db = this.readableDatabase
        db.execSQL("DROP TABLE $payHeading$title")
        //db.rawQuery("DROP TABLE $title",null)
        //db.delete("Pay$title",null,null)

    }
    /**TODO SHOPPING MAIN DATABASES*/
    fun insertMainShopping(shop: Shopping_Main_Data)
    {
        val db = this.readableDatabase
        val values = ContentValues()

        values.put(SHOPPING_TITLE, shop.shopping_Main_Data_title)
        values.put(SHOPPING_DATE,shop.shopping_Main_Data_date)
        values.put(SHOPPING_ITEM_DONE,shop.shopping_Main_Data_itemDone)
        values.put(SHOPPING_ITEM_TOTAL,shop.shopping_Main_Data_itemTotal)
        values.put(SHOPPING_TOTAL, shop.shopping_Main_Data_total)
        db.insert(SHOPPING_TABLE, null,values)
        db.close()
    }
    fun updateMainShoppingTotal(id : String, total : String)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(SHOPPING_TOTAL, total)
        db.update(SHOPPING_TABLE, values, "$SHOPPING_ID = $id", null)

    }
    fun updateMainShopping(shop : Shopping_Main_Data)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(SHOPPING_TITLE, shop.shopping_Main_Data_title)
        values.put(SHOPPING_DATE,shop.shopping_Main_Data_date)
        //values.put(SHOPPING_ITEM_DONE,shop.shopping_Main_Data_itemDone)
        //values.put(SHOPPING_ITEM_TOTAL,shop.shopping_Main_Data_itemTotal)
        db.update(SHOPPING_TABLE,values, "$SHOPPING_ID = ${shop.shopping_Main_Data_id}", null)

    }
    fun getAllMainShopping() : Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $SHOPPING_TABLE",null)
    }
    fun deleteMainShopping(id:Int)
    {
        val db = this.readableDatabase
        db.delete(SHOPPING_TABLE, "$SHOPPING_ID = $id", null)
        db.close()
    }
    fun getMainShoppingID_ForTItle(): String
    {
        val db = this.readableDatabase
        var ans  =""
        val cursor = db.rawQuery("SELECT $SHOPPING_ID FROM $SHOPPING_TABLE ORDER BY $SHOPPING_ID DESC LIMIT 1", null)
        if (cursor.moveToFirst())
        {
            ans=cursor.getString(cursor.getColumnIndex(SHOPPING_ID))
        }
        return ans
    }
    fun setShoppingItems(idTitle:String, purchased: String, total : String)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(SHOPPING_ITEM_TOTAL, total)
        values.put(SHOPPING_ITEM_DONE, purchased)
        db.update(SHOPPING_TABLE, values, "$SHOPPING_ID = $idTitle", null)
        db.close()
    }
    /**TODO SHOPPING LIST  DATABASES*/
    fun createList(id: String)
    {
        val db = this.readableDatabase
        val list ="CREATE TABLE IF NOT EXISTS $shopList$id ($LISTSHOP_ID INTEGER PRIMARY KEY AUTOINCREMENT, $LISTSHOP_TITLE TEXT, $LISTSHOP_PRICE TEXT, $LISTSHOP_QUANTITY TEXT, $LISTSHOP_UNIT TEXT, $LISTSHOP_STATUS TEXT, $LISTSHOP_IDTITLE TEXT, $LISTSHOP_IMAGEURI);"
        db!!.execSQL(list)
        db.close()
    }
    // constructor(title: String, price:String, idTitle:String, quantity :String, status : String, listID: String)
    fun insertList(list_Data: Shopping_List_Data)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(LISTSHOP_TITLE,      list_Data.shopping_List_Data_Title)
        values.put(LISTSHOP_PRICE,      list_Data.shopping_List_Data_Price)
        values.put(LISTSHOP_IDTITLE,    list_Data.shopping_List_Data_IdTitle)
        values.put(LISTSHOP_QUANTITY,   list_Data.shopping_List_Data_Quantity)
        values.put(LISTSHOP_UNIT,       list_Data.shopping_List_Data_Unit)
        values.put(LISTSHOP_STATUS,     list_Data.shopping_List_Data_Status)
        values.put(LISTSHOP_IMAGEURI,   list_Data.shopping_List_Data_Image_Uri)
        db.insert("$shopList${list_Data.shopping_List_Data_IdTitle}",null, values)
        db.close()
    }
    fun getTotalItems(idTitle:String):String{
        val db = this.readableDatabase
        var ans  ="0"
        val cursor = db.rawQuery("SELECT  $L_COUNT FROM $shopList$idTitle", null)
        if (cursor.moveToFirst())
        {
            ans=cursor.getString(cursor.getColumnIndex(L_COUNT))
        }
        return ans
    }
    fun getPurchasedItems(idTitle:String):String{
        val db = this.readableDatabase
        var ans  ="0"
        val cursor = db.rawQuery("SELECT  $L_COUNT FROM $shopList$idTitle WHERE $LISTSHOP_STATUS = 1", null)
        if (cursor.moveToFirst())
        {
            ans=cursor.getString(cursor.getColumnIndex(L_COUNT))
        }
        return ans
    }
    fun deleteList(idTitle: String)
    {
        val db = this.readableDatabase
        db.execSQL("DROP TABLE IF EXISTS $shopList$idTitle")
        db.close()
    }
    fun deleteListItem(idTitle: String, idList : String)
    {
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT $LISTSHOP_IMAGEURI FROM $shopList$idTitle WHERE $LISTSHOP_ID = $idList",null)
        if(cursor.moveToFirst())
        {
            functions.deleteImage(cursor.getString(cursor.getColumnIndex(LISTSHOP_IMAGEURI)))
        }
        db.delete("$shopList$idTitle", "$LISTSHOP_ID = $idList", null)
        db.close()
    }
    fun updateList(shop: Shopping_List_Data)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(LISTSHOP_TITLE, shop.shopping_List_Data_Title)
        values.put(LISTSHOP_PRICE, shop.shopping_List_Data_Price)
        values.put(LISTSHOP_QUANTITY, shop.shopping_List_Data_Quantity)
        values.put(LISTSHOP_UNIT, shop.shopping_List_Data_Unit)
        values.put(LISTSHOP_IMAGEURI, shop.shopping_List_Data_Image_Uri)
        db.update("$shopList${shop.shopping_List_Data_IdTitle}",values,"$LISTSHOP_ID = ${shop.shopping_List_Data_ListID}", null)
        db.close()
    }

    fun updateListCart(idTitle:String, status:String, listID:String)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(LISTSHOP_STATUS, status)
        db.update("$shopList$idTitle", values, "$LISTSHOP_ID = $listID", null)
        db.close()
    }
    fun getStatusOfListItem(idTitle:String, listID:String):String
    {
        val db = this.readableDatabase
        var ans  ="0"
        val cursor = db.rawQuery("SELECT $LISTSHOP_STATUS FROM $shopList$idTitle WHERE $LISTSHOP_ID = $listID", null)
        if (cursor.moveToFirst())
        {
            ans=cursor.getString(cursor.getColumnIndex(LISTSHOP_STATUS))
        }
        return ans
    }
    fun getAllShoppingList(id:String) : Cursor?
    {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $shopList$id ORDER BY $LISTSHOP_ID DESC", null)
    }

    /**TODO THIS IS SETTINGS TABLE FUNCTIONS
     * */
    fun insertSettings( country: String, currency: String, unit :String)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(SETTINGS_COUNTRY, country)
        values.put(SETTINGS_CURRENCY, currency)
        values.put(SETTINGS_UNIT, unit)
        db.insert(SETTINGS_TABLE,null, values)
        db.close()
    }
    fun getSettings() : Cursor
    {
        val db =this.readableDatabase
        return db.rawQuery("SELECT * FROM $SETTINGS_TABLE", null)
    }
    fun updateSettings( country: String, currency: String, unit: String)
    {
        val db = this.readableDatabase
        val values = ContentValues()
        values.put(SETTINGS_UNIT, unit)
        values.put(SETTINGS_CURRENCY, currency)
        values.put(SETTINGS_COUNTRY, country)
        db.update(SETTINGS_TABLE, values, "$SETTINGS_ID = 1", null)
        db.close()
    }

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "MembaMiDatabase.db"
        private val L_COUNT = "COUNT(*)"
        val TABLE_NAME = "Notes"
        val COLUMN_ID = "ID"
        val TITLE = "Title"
        val DESCRIPTION = "Description"
        val MOD_DATE = "MOD_DATE"
        /**TODO THIS IS TABLE FOR BILLS
         * */
        val BILLTABLE       = "Bill"
        val BILL_ID         = "BillID"
        val BILL_TITLE      = "BillTitle"
        val BILL_AMT        =  "BillAmt"
        val BILL_REAPEAT    = "BillRepeat"
        val BILL_BALANCE = "BillBalance"
        val BILL_COLOR      = "BillColor"

        /**TODO THIS IS TABLE FOR PAID BILLS
         * */
        val PAID_ID         = "ID"
        val PAID_AMT        = "PaidAmt"
        val PAID_DUE_AMT    = "PaidDueAmt"
        val PAID_OCCURRENCE = "PaidOccurrence"
        val PAID_OUTSTANDING     = "Outstanding"
        val PAID_DATE       = "PaidDate"

        /**TODO THIS IS THE SECTIONS FOR SHOPPING
         *
         */
        val SHOPPING_TABLE = "SHOPPING_MAIN"
        val SHOPPING_ID     = "ID"
        val SHOPPING_TITLE = "Title"
        val SHOPPING_DATE   = "CreatedDate"
        val SHOPPING_ITEM_DONE = "ItemDone"
        val SHOPPING_ITEM_TOTAL = "ItemTotal"
        val SHOPPING_TOTAL      = "Total"

        /**TODO THIS IS THE SECTIONS FOR LIST OF SHOPPING
         *
         */
        var LISTSHOP_TITLE = "title"
        var LISTSHOP_PRICE = "price"
        var LISTSHOP_IDTITLE ="idTitle"
        var LISTSHOP_QUANTITY = "quantity"
        var LISTSHOP_UNIT      = "unit"
        var LISTSHOP_STATUS = "status"
        val LISTSHOP_ID = "id"
        val LISTSHOP_IMAGEURI = "imageuri"


        /** TODO SETTIGNS DATABASE
         *
         */
        val SETTINGS_TABLE      = "settings_table"
        val SETTINGS_ID         = "id"
        val SETTINGS_COUNTRY    = "country"
        val SETTINGS_CURRENCY   = "currency"
        val SETTINGS_UNIT       = "unit"
    }

}