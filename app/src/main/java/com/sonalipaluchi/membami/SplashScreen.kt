package com.sonalipaluchi.membami

import android.support.v7.app.AppCompatActivity
import android.content.Intent
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreen : AppCompatActivity() {

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds
    val functions = General_Functions()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val db = Retain_Information(this, null)
        try
        {
            var cursor = db.getSettings()
            if(!cursor.moveToNext())
            {
                throw SQLiteException()
            }
            if(cursor.moveToFirst())
            {
                GLB_CUR = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_CURRENCY))
                GLB_UNIT  = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_UNIT))

            }
            startActivity(Intent(this, MainActivity::class.java))
        }
        catch(exc : SQLiteException)
        {
            exc.printStackTrace()


        }
        setContentView(R.layout.activity_splash_screen)
        listener()
    }


    private fun listener() {

        var languageListDDL = arrayOf(
            this.resources.getString(R.string.Jamaica),
            this.resources.getString(R.string.usa),
            this.resources.getString(R.string.Bulgarian),
            this.resources.getString(R.string.eng),
            this.resources.getString(R.string.BulgarianEURO),
            this.resources.getString(R.string.uk)
        )
        var languageList = arrayOf(
            "en",
            "us",
            "bg",
            "en",
            "ir",
            "en"

        )
        var countryList = arrayOf(
            "jm",
            "us",
            "bg",
            "gb",
            "eu",
            "eu"
        )
        //dueperBillsDDL.setSelection(due_per.indexOf( cursor.getString(cursor.getColumnIndex(Retain_Information.BILL_REAPEAT)) ))

        val spinner = this.findViewById<Spinner>(R.id.splash_settings_language_selector_DDL)
        val arrayAdapter = ArrayAdapter(this, R.layout.spinner_row_modifier, languageListDDL)
        spinner?.adapter = arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                splash_settings_currency_Value.text = functions.formatMOney(0.00, languageList[spinner.selectedItemPosition], countryList[spinner.selectedItemPosition])

            }

        }

        splash_settings_button_save.setOnClickListener {

            val db = Retain_Information(this, null)

            db.insertSettings( spinner.selectedItem.toString(),languageList[splash_settings_language_selector_DDL.selectedItemPosition],
                countryList[splash_settings_language_selector_DDL.selectedItemPosition]
            )
            val cursor = db.getSettings()
            if(cursor.moveToFirst())
            {
                GLB_CUR = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_CURRENCY))
                GLB_UNIT = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_UNIT))
            }
            db.close()


            startActivity(Intent(this, MainActivity::class.java))
        }
        splash_settings_button_cancel.setOnClickListener {
            finish()
        }

    }
   /** internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //Initialize the Handler
        mDelayHandler = Handler()

        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

    }


    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }
    */


}
