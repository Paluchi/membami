package com.sonalipaluchi.membami

import android.app.Application

class GlobalVariable : Application(){

    companion object {
        var GLB_UNIT = ""
        var GLB_CUR= ""
    }
    override fun onCreate() {
        super.onCreate()
        GLB_UNIT="en"
        GLB_CUR="jm"
    }

}