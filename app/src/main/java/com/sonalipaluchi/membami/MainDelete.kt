package com.sonalipaluchi.membami

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main_delete.*

class MainDelete : AppCompatActivity() {

    var id = ""
    var type =""
    var misc=""
    val functions = General_Functions()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_delete)
        id   = intent.getStringExtra("DELETE_ID")
        type = intent.getStringExtra("DELETE_TYPE")
        //misc = intent.getStringExtra("DELETE_MISC")

        listeners()

    }

    fun listeners()
    {
        main_delete_button_no.setOnClickListener {
            finish()
        }
        main_delete_button_yes.setOnClickListener {
            validateDelete()
            finish()
        }
    }
    fun validateDelete() {
        val db = Retain_Information(this, null)
        when
        {
            type in getString(R.string.tab_bills)->{
                db.deleteBill(id.toInt())
                db.deletePayments(id)
            }
            type in getString(R.string.tab_shopping) ->
            {
                db.deleteMainShopping(id.toInt())
                val cursor = db.getAllShoppingList(id)
                var address = ""
                if(cursor!!.moveToFirst())
                {
                    address = cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IMAGEURI))
                    functions.deleteImage(address)
                    while (cursor.moveToNext())
                    {
                        address = cursor.getString(cursor.getColumnIndex(Retain_Information.LISTSHOP_IMAGEURI))
                        functions.deleteImage(address)
                    }
                }
                db.deleteList(id)
            }
            type in getString(R.string.tab_notes)->
            {
                db.deleteNote(id.toInt())
            }else->{

                db.deleteListItem(id,type)
                val totalitems = db.getTotalItems(id)
                val purchased = db.getPurchasedItems(id)
                db.setShoppingItems(id, purchased, totalitems)
            }
        }
        db.close()


    }

}
