package com.sonalipaluchi.membami
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.text.HtmlCompat
import android.support.v4.view.PagerAdapter
import bills_package.Bills
import shopping_package.Shopping
import notes_package.Notes
import java.util.ArrayList
import android.view.Menu
import android.view.MenuItem


class MainActivity : AppCompatActivity() {

    private var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    val functions = General_Functions()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = HtmlCompat.fromHtml("<font color='#8B4513'>${supportActionBar?.title} </font>", HtmlCompat.FROM_HTML_MODE_LEGACY)

        functions.getAllPermissions(this)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA),
               0)

        }


        viewPager = findViewById(R.id.viewpager) as ViewPager
        setupViewPager(viewPager!!)
        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                onPageSelected(position)
            }
            override fun onPageSelected(position: Int) {

            }
        })
        tabLayout = findViewById(R.id.tabs) as TabLayout
        tabLayout!!.setupWithViewPager(viewPager)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item!!.itemId)
        {
            R.id.menu_main_settings ->{
                startActivity(Intent(this, Main_Settings::class.java))

            }
            R.id.menu_add_note_cancel->{

            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        //adapter.addFragment(Home(), getString(R.string.tab_home).capitalize())
        adapter.addFragment(Bills(), getString(R.string.tab_bills).capitalize())
        adapter.addFragment(Shopping(), getString(R.string.tab_shopping).capitalize())
        adapter.addFragment(Notes(), getString(R.string.tab_notes).capitalize())
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
           return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_UNCHANGED
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }


}

