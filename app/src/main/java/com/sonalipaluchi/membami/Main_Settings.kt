package com.sonalipaluchi.membami

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import kotlinx.android.synthetic.main.activity_main__settings.*

class Main_Settings : AppCompatActivity() {

    val functions = General_Functions()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main__settings)

        listeners()
    }

    private fun populateDDL()
    {
        val db = Retain_Information(this@Main_Settings, null)

        var languageListDDL = arrayOf(this.resources.getString(R.string.Jamaica),
            this.resources.getString(R.string.usa),
            this.resources.getString(R.string.Bulgarian),
            this.resources.getString(R.string.eng),
            this.resources.getString(R.string.BulgarianEURO),
            this.resources.getString(R.string.uk))
        var currencyList = arrayOf(
            "en",
            "us",
            "bg",
            "en",
            "ir",
            "en"
        )
        var  unitList = arrayOf(
            "jm",
            "us",
            "bg",
            "gb",
            "eu",
            "eu"
        )

        val spinner = this.findViewById<Spinner>(R.id.settings_language_selector_DDL)
        val arrayAdapter= ArrayAdapter(this, R.layout.spinner_row_modifier ,languageListDDL)
        spinner?.adapter = arrayAdapter

        val cursor = db.getSettings()
        if(cursor.moveToFirst()) {
            var country = cursor.getString(cursor.getColumnIndex(Retain_Information.SETTINGS_COUNTRY))
            val position = languageListDDL.indexOf(country)
            spinner.setSelection(position)
        }

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                settings_currency_Value.text = functions.formatMOney(0.00, currencyList[spinner.selectedItemPosition], unitList[spinner.selectedItemPosition])

            }

        }

        settings_button_save.setOnClickListener {

            db.updateSettings(spinner.selectedItem.toString(),currencyList[spinner.selectedItemPosition], unitList[spinner.selectedItemPosition])

            GLB_CUR = currencyList[spinner.selectedItemPosition]
            GLB_UNIT = unitList[spinner.selectedItemPosition]
            finish()
        }
        settings_button_cancel.setOnClickListener {

            finish()
        }

        db.close()


    }

    private fun listeners()
    {

        populateDDL()
    }

}
