package com.sonalipaluchi.membami
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_images_camera.*

class Images_Camera : AppCompatActivity()
{
    //private var btn: Button? = null
   // private var imageview: ImageView? = null
    val functions = General_Functions()
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreate(savedInstanceState:Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images_camera)

       // btn
       // iv

        btn!!.setOnClickListener { showPictureDialog() }

    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val permisson = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        if (permisson == PackageManager.PERMISSION_GRANTED) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }else
        {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.CAMERA),
                0)
        }

       // if(functions.getPermission(this))
        //{
//
       // }

    }

    public override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /* if (resultCode == this.RESULT_CANCELED)
         {
         return
         }*/
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                val contentURI = data.data
                try
                {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)
                    Log.e("ERROR", path)
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
                    functions.bitmapToFile(bitmap,this)
                    Log.e("Path", functions.bitmapToFile(bitmap,this).toString())
                    iv!!.setImageBitmap(bitmap)
                    val sonali = BitmapFactory.decodeFile(functions.bitmapToFile(bitmap,this).toString())
                    testiv.setImageBitmap(sonali)

                    /**Glide.with(this)
                        .load(path)
                        .apply(
                            RequestOptions()
                                .placeholder(R.drawable.membami_images_icon_vanilla)
                                .circleCrop()
                        )
                        .error(R.drawable.membami_images_icon_vanilla)
                        .into(testiv)
                    */


                }
                catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
           // val temp = thumbnail.toString()
            val uri = functions.bitmapToFile(thumbnail, this)

            Log.e("THUMBNAIL", uri.toString())
            Log.e("#####THUTL", uri.toString())
            //val imagebytes = Base64.decode(temp,0)
           // val image = BitmapFactory.decodeByteArray(imagebytes,0,imagebytes.size)

            iv!!.setImageBitmap(thumbnail)
            val sonali = BitmapFactory.decodeFile(functions.bitmapToFile(thumbnail,this).toString())
            testiv.setImageBitmap(sonali)

           /** saveImage(thumbnail)
            Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
            Glide.with(this)
                .load(uri)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.membami_images_icon_vanilla)
                        .circleCrop()
                )
                .error(R.drawable.membami_images_icon_vanilla)
                .into(testiv)*/
        }
    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString())
        // have the object build the directory structure, if needed.
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }

}