package com.sonalipaluchi.membami

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.inputmethod.InputMethodManager
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_CUR
import com.sonalipaluchi.membami.GlobalVariable.Companion.GLB_UNIT
import org.jetbrains.annotations.Nullable
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.StringBuilder
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.*


class General_Functions: AppCompatActivity() {

    fun hideKeyBoard(activity: Activity)
    {
        val view = activity.currentFocus
        view?.let { v ->
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.let { it.hideSoftInputFromWindow(v.windowToken, 0) }
        }
    }
    fun billDuePer(context: Context) : Array<String>
    {
        return arrayOf(context.resources.getString(R.string.yearly),
            context.resources.getString(R.string.quarterly),
            context.resources.getString(R.string.monthly),
            context.resources.getString(R.string.fortnight),
            context.resources.getString(R.string.weekly),
            context.resources.getString(R.string.daily))
    }


    fun getUnits(context: Context) : Array<String>
    {
        return arrayOf(
            context.resources.getString(R.string.kilogram),
            context.resources.getString(R.string.gram),
            context.resources.getString(R.string.litre),
            context.resources.getString(R.string.units),
            context.resources.getString(R.string.pounds),
            context.resources.getString(R.string.metre),
            context.resources.getString(R.string.centimetre),
            context.resources.getString(R.string.feet)
        )
    }
    fun symbolChecker(number: String): String {
        val symbols = arrayOf(
            "$",
            "¥",
            "€",
            "£",
            "₩",
            "Rs",
            "฿",
            "₫",
            "₹",
            "Rp",
            "HK\$",
            "元",
            "৳",
            "R\$",
            "₦",
            "₪",
            "د.إ",
            "лв",
            "BGN",
            " лв."
        )
        var i = 0
        var temp = number
        while (i < symbols.size) {
            if (temp.contains(symbols[i], true)) {
                val index = temp.indexOf(symbols[i])
                temp = replaceChar(temp, ' ', index)
            }

            i++
        }

        return temp.trim()
    }
    fun checkExternalStorage() :Boolean
    {
        var result = false
        //val state = Environment.getExternalStorageState()

        if(Environment.MEDIA_MOUNTED == Environment.getExternalStorageState())
            result = true

        return result
    }
    fun deleteImage(imagePath:String)
    {
        if(!imagePath.isNullOrEmpty())
        {
            val file = File(imagePath)
            if(file.exists()) {
                if (file.delete()) {
                    System.out.println("file Deleted :" + imagePath);
                    Log.e("ComPlETED", "Deleted $imagePath")
                } else {
                    System.out.println("file not Deleted :" + imagePath);
                    Log.e("ERROR", "NOT DELETED $imagePath")
                }
            }
        }
    }
    fun   rotateImage( img: Bitmap,  degree :Int) :Bitmap{
        var  matrix =Matrix()
        matrix.postRotate(degree.toFloat());
        var rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        //img.recycle()
        return rotatedImg;
    }


    fun getOrientation(activity: Activity, imageUri :Uri) : Int
    {
        var input = activity.contentResolver.openInputStream(imageUri)
        var exf= ExifInterface(input)
        var ans = exf.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        return ans
    }
    fun getDegrees(value : Int):Int
    {
        var result = 0
        when (value)
        {
            ExifInterface.ORIENTATION_ROTATE_90-> result = 90
            ExifInterface.ORIENTATION_ROTATE_180-> result =180
            ExifInterface.ORIENTATION_ROTATE_270-> result =270
        }
        return result
    }
    /**
    fun applyDim(parent: ViewGroup, dimAmount: Float) {
        val dim = ColorDrawable(Color.BLACK)
        dim.setBounds(0, 0, parent.width, parent.height)
        dim.alpha = (255 * dimAmount).toInt()

        val overlay = parent.overlay
        overlay.add(dim)
    }

    fun clearDim(parent: ViewGroup) {
        val overlay = parent.overlay
        overlay.clear()
    }
    fun PopupWindow.dimBehind() {
        val container = contentView.rootView
        val context = contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.8f
        wm.updateViewLayout(container, p)
    }

    fun getRightAngleImage(photoPath: String): String {

        try {
            val ei = ExifInterface(photoPath)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            var degree = 0

            when (orientation) {
                ExifInterface.ORIENTATION_NORMAL -> degree = 0
                ExifInterface.ORIENTATION_ROTATE_90 -> degree = 90
                ExifInterface.ORIENTATION_ROTATE_180 -> degree = 180
                ExifInterface.ORIENTATION_ROTATE_270 -> degree = 270
                ExifInterface.ORIENTATION_UNDEFINED -> degree = 0
                else -> degree = 90
            }

            return rotateImage(degree.toFloat(), photoPath)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return photoPath
    }
    private fun rotateImage(degree: Float, imagePath: String): String {

        if (degree <= 0) {
            return imagePath
        }
        try {
            var b = BitmapFactory.decodeFile(imagePath)

            val matrix = Matrix()
            if (b.width > b.height) {
                matrix.setRotate(degree)
                b = Bitmap.createBitmap(
                    b, 0, 0, b.width, b.height,
                    matrix, true
                )
            }

            val fOut = FileOutputStream(imagePath)
            val imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1)
            val imageType = imageName.substring(imageName.lastIndexOf(".") + 1)

            val out = FileOutputStream(imagePath)
            if (imageType.equals("png", ignoreCase = true)) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out)
            } else if (imageType.equals("jpeg", ignoreCase = true) || imageType.equals("jpg", ignoreCase = true)) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out)
            }
            fOut.flush()
            fOut.close()

            b.recycle()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return imagePath
    }
    fun saveExternal(image : Bitmap, filename :String) : String
    {
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/$filename")
        if (!myDir.exists()) {
            myDir.mkdirs()
        }
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        val fname = "Image-$n.jpg"
        val file = File(myDir, fname)
        if (file.exists())
            file.delete()
        try {
            val out = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.flush()
            out.close()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return " "
    }*/

    fun saveInternal(image: Bitmap, activity: Activity) : String
    {

        val path = activity.getDir("Images", Context.MODE_PRIVATE)

        //val file = File(path, "${UUID.randomUUID()}.jpg")
        val file = File(path, "${Calendar.getInstance().timeInMillis}.jpg")
        try {
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(file)

            // Compress the bitmap
            image.compress(Bitmap.CompressFormat.JPEG, 100, stream)

            // Flush the output stream
            stream.flush()

            // Close the output stream
            stream.close()
            Log.e("Image saVFRed",file.absolutePath)

            // Log.e("PATH ISDS", sdcardPath)
            //Log.e("External Directory",  System.getenv("SECONDARY_STORAGE"))
        } catch (e: IOException){ // Catch the exception
            e.printStackTrace()
            Log.e("Error to save image.", "asdsadas")
        }

        // Return the saved image path to uri
        return file.absolutePath
    }

    fun scaleBitmap( bm: Bitmap, maxWidth : Int,  maxHeight: Int) : Bitmap{
        var width = bm.width
        var height = bm.height

        if (width > height) {
            // landscape
            var ratio = width / maxWidth
            width = maxWidth
            height = height / ratio
        } else if (height > width) {
            // portrait
            var ratio = height / maxHeight
            height = maxHeight
            width = width / ratio
        } else {
            // square
            height = maxHeight
            width = maxWidth
        }


        return Bitmap.createScaledBitmap(bm, width, height, true)
    }


    fun getRandomColor(): Int {
        var colorArray = arrayOf(
            com.sonalipaluchi.membami.R.color.sonaliParchment,
            //com.example.membami.R.color.sonaliCapePalliser,
            //com.example.membami.R.color.sonaliGrey,
            com.sonalipaluchi.membami.R.color.sonaliVanilla,
            com.sonalipaluchi.membami.R.color.sonaliWhite,
            // com.example.membami.R.color.sonaliTrinidad,
            com.sonalipaluchi.membami.R.color.sonaliTowerGray,
            //com.example.membami.R.color.sonaliWildWaterMelon,
            com.sonalipaluchi.membami.R.color.sonaliYellow,
            com.sonalipaluchi.membami.R.color.sonaliMongoose
        )


        return colorArray[(0 until colorArray.size - 1).shuffled().last()]


    }
    fun getAllPermissions(activity : Activity)
    {
        val permissions = arrayOf(android.Manifest.permission.INTERNET,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE

            )
        ActivityCompat.requestPermissions(activity, permissions,0)
    }
    fun getPermission(activity: Activity, type: String) : Boolean
    {
        var result =false
        val permisson = ContextCompat.checkSelfPermission(activity, type)
        if (permisson != PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(activity,
                arrayOf(type),
                0)

            result= true

        }else
        {
            result =false

        }
        return result
    }

    fun getDate(): String {
        val rightNow = Calendar.getInstance()
        /**val month = rightNow.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
        val day = rightNow.get(Calendar.DAY_OF_MONTH)
        val year = rightNow.get(Calendar.YEAR)
        //rightNow.get(Calendar.MONTH)
        val date = "$month $day, $year" */
        return (rightNow.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())) + " " + rightNow.get(
            Calendar.DAY_OF_MONTH
        ) + " ," + rightNow.get(Calendar.YEAR)
    }

    fun formatMOney(value: Double, language: String, country: String): String {
        val format = NumberFormat.getCurrencyInstance(Locale(language, country))
        //val format = NumberFormat.getCurrencyInstance(Locale.UK)
        format.minimumFractionDigits = 2
        format.maximumFractionDigits = 2
        format.roundingMode = RoundingMode.HALF_EVEN
        return format.format(value)
    }

    fun getFloat(value: String): Float {

        var trimmed = value.trim()
        var nosign = symbolChecker(trimmed)

        var noLetter = nosign.takeWhile { !it.isLetter() }
        Log.e("Letter", noLetter)
        noLetter = noLetter.trim()

        var index = noLetter.lastIndexOf(',')
        Log.e("THIS IS THER INDEX", index.toString())
        if (index != -1) {
            if ((index + 3) == noLetter.length) {
                noLetter = replaceChar(noLetter, '.', index)
            }
        }
        return noLetter.toFloat()

    }

    fun replaceChar(str: String, ch: Char, index: Int): String {
        val myString = StringBuilder(str)
        myString.setCharAt(index, ch)
        return myString.toString()
    }

    fun heavyFunction(value : String): String
    {
       // var result = String()
        var temp1 = getFloat(formatMOney(value.toDouble(), GLB_CUR,GLB_UNIT))

        return addTralingzeros(temp1.toString())
    }

    fun addTralingzeros(ans :String):String
    {
        var temp= ans

        if(!ans.isNullOrBlank() || !ans.isNullOrEmpty())
        {
            val length = ans.length

            if (ans.contains('.'))
            {
                if(((ans.indexOf('.')) + 2) == length )
                {
                    temp  = "${ans}0"
                }else if ( (((ans.indexOf('.')) + 3) == length) || ( ((ans.indexOf('.')) + 3) != length ))
                {
                    var i =0
                    while(i<(ans.length+3))
                    {
                        //temp[i] = ans[i]
                        i++
                    }



                }
            }else if (!ans.contains('.'))
            {
                temp = "$ans.00"

            }
           /** val index = temp.indexOf('.')
            if(index != -1 && (index +3) != temp.length )
            {
                var result = temp

            }*/
        }

        return temp
    }
        @Nullable
    fun bitmapToFile(bitmap: Bitmap, context: Context): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images",Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpg")


        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e: IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }

    /**  //fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)


    //fun setEditTextValue(title :String)
    //{
    //    Editable.Factory.getInstance().newEditable(
   //         cursor.getString(cursor.getColumnIndex(Retain_Information.DESCRIPTION))
   // }
    fun validColor(color:String) : Boolean
    {
        var valid = false
        try {
            val color  = Color.parseColor(color)
            valid = true

        }catch (e : IllegalArgumentException)
        {
            valid = false
        }

        return valid

    }

    fun refreshFragment(frag : Fragment)
    {

        fragmentManager.beginTransaction().detach(frag).attach(frag).commit()

        println("<<<<<<<")
        println("")
        println("ANS CALLED")
        println("")
        println(">>>>>>>")
    }
    fun PerfectDecimal(str: String, MAX_BEFORE_POINT: Int, MAX_DECIMAL: Int): String {
        var str = str
        if (str[0] == '.') str = "0$str"
        val max = str.length

        var rFinal = ""
        var after = false
        var i = 0
        var up = 0
        var decimal = 0
        var t: Char
        while (i < max) {
            t = str[i]
            if (t != '.' && after == false) {
                up++
                if (up > MAX_BEFORE_POINT) return rFinal
            } else if (t == '.') {
                after = true
            } else {
                decimal++
                if (decimal > MAX_DECIMAL)
                    return rFinal
            }
            rFinal = rFinal + t
            i++
        }
        return rFinal
    }

    */



    /**fun floatlizeString(mny : String): String
    {
        var temp = mny.trim()
        var index = temp.lastIndexOf(',')
        temp.replace(',', '.',true)
        //var temp2 = temp.take(temp.indexOf('.') + 2)
        var temp2 = temp.takeWhile { !it.isLetter() }

        temp2.replace(",", ".",true)
        var ans = temp2.trim()
        ans.replace(",",".")

        var result = ans
        var builder = StringBuilder()
        var indexT = ans.indexOf(",")
        if (ans.contains(",")) {
            var i = 0
            while(i < ans.length) {
                if (indexT == i) {
                    builder.append(".")
                } else {
                    builder.append(ans[i])
                }
                i = i +1

            }

            result = builder.toString()
        }

        Log.e("THIS FINAL RESULT <$$>", result)





        return result
    }

    fun replaceMe(string: String):String
    {
        var value = string.replace("$","")
        return value
    }*/






    /**TODO
     * Editable.Factory.getInstance().newEditable(savedString)
     *
     * fun replaceSpace(value : String):String
    {
    val nae = value.replace(" ","_")

    return nae
    }
     *
     */
}

